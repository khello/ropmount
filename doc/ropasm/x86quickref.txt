add      dest, value             |  Add a value to a register.
mov      dest, src               |  Mov a register to another (doesn't preserve src).
call     name, args              |  Call a function with given args.
cpy      dest, src               |  Cpy a register to another (preserve src).
deref    register                |  Dereference a register => mov reg, [reg].
int      intnb, args             |  Exec an interupt.
clean    nb_dword                |  Clean X dword on the stack.
pecall   name, autoclean, args   |  Call Pe-like (deref addresse before call).
retreg   register                |  Return on a register: may not preserv the register.
set      register, value         |  Set a register to a value.
strstore dest, string            |  Store a string in memory.
write    dest, src               |  Write value into an addr.
