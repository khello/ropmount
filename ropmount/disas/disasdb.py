#!/usr/bin/python2


class DisasDB(object):
    """
        A database of InstrDisas:
        Efficient to extract a lot of GadgetDisas from a simple block of data
    """
    def __init__(self):
        raise NotImplementedError("Mother class DisasDB isn't a valid Dissembler")

    def disas(self, vaddr):
        """Return disas of instr at vaddr if valid : None else"""
        raise NotImplementedError()
