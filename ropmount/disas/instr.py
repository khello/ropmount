#!/usr/bin/python2

class InstrDisas(object):
    def stack_consumption(self):
        """Return the number of byte of stack comsumed by the instruction"""
        raise NotImplementedError()

    def is_valid_rop(self):
        """Return True if Instr is okay to appear in a ROP"""
        raise NotImplementedError()

    def invalid_register(self):
        """Return a RegSet of invalidated Register"""
        raise NotImplementedError()
