#!/usr/bin/python2

class Register(object):
    def __hash__(self):
        raise NotImplementedError("{0} register need a valid __hash__ implem"
                                    .format(self.__class__))

    def __eq__(self, other):
        raise NotImplementedError("{0} need a valid __hash__ implem"
                .format(self.__class__))

    def __ne__(self, other):
        return not self == other


class MemAccess(object):
    pass

class Immediat(object):
    #May have check on value later
    def __init__(self, value, size):
        self.size = size
        if isinstance(value, (long, int)):
            self.value = value
        else:
            self.value = int(value, 0)

    def __eq__(self, other):
        return self.value == other.value and self.size == other.size

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        """
            Hash of a Immediat is the hash of the tuple (value, size)
        """
        return hash((self.value, self.size))

    def __str__(self):
        return hex(self.value)

    def __repr__(self):
        return "{0}({1},{2})".format(self.__class__.__name__, self.value, self.size)
