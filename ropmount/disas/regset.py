#!/usr/bin/python2

from .type import Register


class RegSet(set):
    """ Represent a set of registers with inclusion
        of sub-register
    """
    #Dict of dependance : fill by subclasses
    #See file ./x86/x86regset.py for example
    register_dep = {}
    #The type of Register contained by the RegSet
    register_type = Register

    def add(self, reg):
        if  not isinstance(reg, Register):
            raise ValueError("Adding a Non Register <{0}> to RegSet".format(reg))
        super(RegSet, self).add(reg)
        if reg in self.register_dep:
            for in_reg in self.register_dep[reg]:
                self.add((in_reg))

    def __init__(self, iterable=()):
        super(RegSet, self).__init__(iterable)
        #needed for recurssif add
        for reg in iterable:
            self.add(reg)

    def extend(self, iterable):
        try:
            for reg in iterable:
                self.add(reg)
        except TypeError:
            raise TypeError("{0} is not iterable".format(iterable.__class__))

