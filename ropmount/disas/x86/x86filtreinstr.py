#!/usr/bin/python2

from ..type import Register, MemAccess, Immediat

CtoM = (MemAccess, Immediat)
CtoR = (Register, Immediat)
MtoR = (Register, MemAccess)
RtoM = (MemAccess, Register)
RtoR = (Register, Register)

simple_double_args = [RtoR, CtoR, CtoM, MtoR, RtoM]
simple_double_noconst = [RtoR, MtoR, RtoM]
CorR = [(Register,), (Immediat,)]
CorRorM = [(Register,), (Immediat,), (MemAccess,)]
NoArg = [()]

#Represent the mnemonic we can filter and their arguments
#Used by asmparser in ropfinding
x86_filter_instrs  = {
        #2 args instr
        'add' : simple_double_args,
        'and' : simple_double_args,
        'mov' : simple_double_args,
        'or' : simple_double_args,
        'sub' : simple_double_args,
        'xchg' : simple_double_noconst,
        'xor' : simple_double_args,
        #0/1 arg instr
        'call': CorRorM,
        'jmp': CorRorM,
        'dec' : CorR,
        'inc' : CorR,
        'int' : [(Immediat,)],
        'neg' : [(Register,)],
        'not' : [(Register,)],
        'pop' : [(Register,)],
        'popad' : NoArg,
        'push' : CorRorM,
        'pushad' : NoArg,
        'lea' : [MtoR],
        'leave' : NoArg,
        'ret' : NoArg,
        'retn' : [(Immediat,)],
             }
