#!/usr/bin/python2

import BeaEnginePython as Bea

from ropmount.disas.disasdb import DisasDB
from .x86instr import X86InstrDisas

class X86DisasDB(DisasDB):
    """
        A database of InstrDisas:
        Efficient to extract a lot of GadgetDisas from a simple block of data
    """
    def __init__(self, code_block, vaddr):
        size = len(code_block)
        self.vaddr = vaddr
        self.end_vaddr = vaddr + size
        #Add garbage at the end to disas after the code_bock limit
        self.buffer = Bea.create_string_buffer(code_block + "\x00" * 15, size + 15)
        self.buffer_addr = Bea.addressof(self.buffer)
        # Addr => FixDisasm
        self.database = {}

    def disas(self, addr):
        """Return disas of instr at addr if valid : None else"""
        if addr in self.database:
            return self.database[addr]
        if not self.vaddr <=  addr < self.end_vaddr:
            raise ValueError("Disas addr not in segment limits")
        res = self._get_fixdisasm(addr)
        #If disas go after code block : ignore
        if not res or addr + res.size > self.end_vaddr:
            res = None
        self.database[addr] = res
        return res

    #addr not in database
    def _get_fixdisasm(self, addr):
        """Disas instruction at addr : return None if disas failed"""
        instr = Bea.DISASM()
        instr.EIP = self.buffer_addr + (addr - self.vaddr)
        size = Bea.Disasm(Bea.addressof(instr))
        if size < 0:
            return None
        return X86InstrDisas(instr, size, addr)
