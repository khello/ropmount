#!/usr/bin/python2

from ..regset import RegSet
from x86type import X86Register

class X86RegSet(RegSet):
    register_dep = {
         X86Register("EAX") : map(X86Register,["AX"]),
         X86Register("AX") : map(X86Register,["AH","AL"]),
         X86Register("EBX") : map(X86Register,["BX"]),
         X86Register("BX") : map(X86Register,["BH","BL"]),
         X86Register("ECX") : map(X86Register,["CX"]),
         X86Register("CX") : map(X86Register,["CH","CL"]),
         X86Register("EDX") : map(X86Register,["DX"]),
         X86Register("DX") : map(X86Register,["DH","DL"]),
         X86Register("EBP") : map(X86Register,["BP"]),
         X86Register("ESP") : map(X86Register,["SP"]),
         }
    register_type = X86Register
