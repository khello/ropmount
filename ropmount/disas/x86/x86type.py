#!/usr/bin/python2

from ..type import Register, MemAccess, Immediat
import BeaEnginePython as Bea

Bea_RegValue =  {
             Bea.REG0: "EAX",
             Bea.REG1: "ECX",
             Bea.REG2: "EDX",
             Bea.REG3: "EBX",
             Bea.REG4: "ESP",
             Bea.REG5: "EBP",
             Bea.REG6: "ESI",
             Bea.REG7: "EDI",
           }

class X86Register(Register):
    DEFAULT_REG_SIZE  = 4
    valid32 = ["EAX", "ECX", "EDX", "EBX", "ESP", "EBP", "ESI", "EDI"]
    valid = list(valid32)
    valid += ["AX", "AH", "AL"]
    valid += ["BX", "BH", "BL"]
    valid += ["CX", "CH", "CL"]
    valid += ["DX", "DH", "DL"]
    valid += ["DI", "SI"]
    valid += ["BP"]
    valid += ["SP"]
    valid += ["NOREG"]


    def __init__(self, reg):
        reg = reg.upper()
        if reg not in self.valid:
            raise ValueError("Not a valid X86Register : <{0}>".format(reg))
        self.value = reg
        if self.value in X86Register.valid32:
            self.size = self.DEFAULT_REG_SIZE
        else:
            if self.value[-1] in ["X", "I", "P"]:
                self.size = 2
            else:
                self.size = 1
        if self.value == "NOREG":
            self.size = 0

    @staticmethod
    def from_bea_argtype(arg):
        if arg.ArgType & 0xffff0000 != Bea.REGISTER_TYPE + Bea.GENERAL_REG:
            raise ValueError("Register from Bea with bad ArgType")
        try:
            if arg.ArgMnemonic:
                return X86Register(arg.ArgMnemonic)
            if arg.ArgSize != 32:
                # This case is not for simple rop instruction
                return -1
            return X86Register(Bea_RegValue[arg.ArgType & 0xffff])
        except KeyError:
            # This case is not for simple rop instruction
            #print "Reg from Bea : KeyError[{0}]".format(hex(arg.ArgType & 0xffff))
            return -1

    @staticmethod
    def listreg_from_bea(value):
        """Extract list of conserned register from a 'or' of Bea Register Flags"""
        res = []
        if value & 0xffff0000 != Bea.REGISTER_TYPE + Bea.GENERAL_REG:
            print "FAIL CALL"
            return res
        value = value & 0xffff
        for nb in Bea_RegValue:
            if value & nb:
                res.append(X86Register(Bea_RegValue[nb]))
        return res

    def __len__(self):
        return self.size

    def __eq__(self, other):
        if other == None:
            return False
        if not isinstance(other, X86Register):
            raise ValueError("Can't compare {0} and {1}".format(self.__class__, other))
        return self.value == other.value

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash(self.value)

    def __str__(self):
        return self.value

    def __repr__(self):
        return "x86Register('{0}')".format(self.value)


class X86Immediat(Immediat):
    def __init__(self, value, size=X86Register.DEFAULT_REG_SIZE):
        super(X86Immediat, self).__init__(value, size)


class X86MemAccess(MemAccess):
    valid_scale = [1, 2, 4, 8]

    def __init__(self, base=None, index=None, scale=None, displacement=None):
        if index and not scale or not index and scale > 1:
            raise ValueError("MemAcces with index={0} and scale={1}".format(index, scale))
        if scale and scale not in X86MemAccess.valid_scale:
            raise ValueError("MemAcces invalid scale : {0}".format(scale))
        self.scale = scale
        self.displacement = displacement
        if base:
            self.base = X86Register(Bea_RegValue[base])
        else:
            self.base = X86Register("NOREG")
        if index:
            self.index = X86Register(Bea_RegValue[index])
        else:
            self.index = X86Register("NOREG")

    @staticmethod
    def from_bea_argtype(arg):
        if not arg.ArgType & 0xffff0000 == Bea.MEMORY_TYPE:
            raise ValueError("X86Memacces : invalid argtype")
        mem = arg.Memory
        return X86MemAccess(mem.BaseRegister, mem.IndexRegister, mem.Scale, mem.Displacement)
