#!/usr/bin/python2

class GadgetDisas(object):
    """
       The disas of a gadget
       Multiples InstrDisas + a virtual address
    """

    def __init__(self, instrs):
        self.instrs = instrs
        self.vaddr = instrs[0].vaddr
        self.descr = ";".join(map(str, instrs))

    def __iter__(self):
        """Itering on a gadget is iterting on his list of instruction"""
        return iter(self.instrs)

    def is_valid_rop(self, begin):
        """A gadget is valid for rop if all its instruction are valid"""
        return all([instr.is_valid_rop() for instr in self.instrs[begin:]])

    def stack_consumption(self):
        """
            stack consumption of a gadget is the sum of the instruction stack consumption
        """
        return sum([instr.stack_consumption() for instr in self.instrs])

    def invalid_register(self, begin=0):
        """
            the registers invalidated by a gadget is the union
            of the registers invalidated by the instructions
        """
        res = set()
        for instr in self.instrs[begin:]:
            res |= instr.invalid_register()
        return res
