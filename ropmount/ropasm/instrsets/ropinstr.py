#!/usr/bin/python2

from abc import ABCMeta
from ropmount.disas.type import Immediat, Register
from ..assemblerexception import AssemblerException

class ImmedOrReg(object):
    """
        A Type of RopInstr Argument
        Indicate that the Argument could be an Immediat or a Register of the
        RopInstr Archi
    """
    __metaclass__ = ABCMeta

    def __init__(self, RegisterClass, ImmediatClass):
        self.regclass = RegisterClass
        self.immediatclass= ImmediatClass

    def parse(self, obj):
        try:
            x = self.immediatclass(obj)
            return x
        except ValueError:
            try:
                x = self.regclass(obj)
                return x
            except ValueError:
                raise ValueError("Not A valid Immediat or Reg : <{0}>".format(obj))
ImmedOrReg.register(Immediat)
ImmedOrReg.register(Register)


#First addr in the stack is the addr of the gadget
class RopStack(object):
    """
        A stack : the payload of every RopInstr
    """
    next_addr = -1

    def __init__(self):
        self.values = []

    def append(self, value):
        """
            Append an Immediat to the stack
        """
        if not isinstance(value, Immediat):
            raise ValueError("add {0} of type {1} in stack".format(value, type(value)))
        self.values.append(value)

    def add_garbage(self, nb_bytes):
        """
            Add nb_bytes bytes of garbage on the stack
        """
        self.values.extend(map(lambda tup : Immediat(*tup), [(0x42, 1)] * (nb_bytes)))

    def add_clean(self, nb_bytes):
        """
            Add next_addr indicator then nb_bytes of garbage
            Do nothing if nb_bytes == 0
            Useful for 'retn 0xc'
        """
        if not nb_bytes:
            return
        self.append(Immediat(RopStack.next_addr, 0))
        self.add_garbage(nb_bytes)

    # Last instr of gadget is a ret : args[0] in the size of cleaned args
    def append_gadget(self, gadget, values):
        """
            Append a Gadget to the stack : take care of garbage value
        """
        used = sum(map(lambda v: v.size, values))
        before_ret = gadget.stack_consumption() - used - gadget.instrs[-1].stack_consumption()
        if before_ret < 0:
            raise ValueError("Can't clean neg size")
        self.values.append(gadget.vaddr)
        self.values.extend(values)
        self.add_garbage(before_ret)
        self.add_clean(gadget.instrs[-1].args[0].value)

    def append_function(self, function_addr, values):
        """
            Append a function call to the stack
        """
        self.values.append(function_addr)
        self.values.append(Immediat(RopStack.next_addr, 0))
        self.values.extend(values)

    def merge(self, other):
        """
            Merge a ropstack to the current one
        """
        if len(other.values) == 0:
            return
        if any([immed.value == RopStack.next_addr for immed in self.values]):
            other_addr = other.values[0]
            self.values = [v if v.value != RopStack.next_addr else other_addr for v in self.values]
            self.values.extend(other.values[1:])
        else:
            self.values.extend(other.values)

    def __len__(self):
        return sum(map(lambda v:v.size, self.values))


class RopInstr(object):
    """
        High level Rop Instruction
    """
    #mnemonic of the instrcution
    mnemo = ""
    #templates that the instruction need
    templates = []
    #gadgets that match the previous templates
    gadgets = [[]]
    #arguments that ropinstr is expecting
    args = ()
    #infomation about the instruction arguments
    args_doc = ()
    #minimal number of args
    args_min = 0

    @classmethod
    def assemble(cls, ass, args, no_regs):
        """
            Assemble a ROP instruction
            return a RopInstr object if successful
            None Else
        """
        if len(args) < cls.args_min:
            raise AssemblerException("Not enough argument for instruction <{0}> : min is {1}".format(
                cls.mnemo, cls.args_min))
        #args verification : is also done in raw_assemble
        #if normal flow is followed : won't be raised
        if not all((isinstance(arg,expected_type) for arg, expected_type in zip(args, cls.args))):
            raise ValueError("Bad args")

    def __init__(self):
        self.stack = RopStack()

    def merge(self, other):
        """
            Merge with another RopInstr
            Just merging the stack
        """
        self.stack.merge(other.stack)

    def __str__(self):
        return "<Raw RopInstr>"

    def __len__(self):
        """
            len of a RopInstr if the len of its stack
        """
        return len(self.stack)

    #if we have an instance : object found
    #so not false
    def __nonzero__(self):
        return True

class MultipleInstr(RopInstr):
    """
        A RopInstr to merge with other ropinstr
        Useful for nice print of RopInstr
    """
    debug_head = "Multiple {0}:"
    debug = ""

    def merge(self, other):
        super(MultipleInstr, self).merge(other)
        self.debug = self.debug + "\n  "+ str(other).replace('\n', '\n  ')

    def __str__(self):
        return self.debug_head.format(hex(len(self.stack))) + self.debug

    def __copy__(self):
        m = MultipleInstr()
        m.debug = self.debug
        m.stack = RopStack()
        m.stack.values = list(self.stack.values)
        if not self.debug_head is MultipleInstr.debug_head:
            m.debug_head = self.debug_head
        return m

class BestLen(object):
    """
        Assign value only if new gadget is better that older
        based on len(RopInstr)
    """
    debug = 0
    def __init__(self, value):
        self._value = value

    def getv(self):
        return self._value

    def setv(self, value):
        if self._value is None:
            self._value = value
        if value is None:
            return
        if len(value) < len(self._value):
            self._value = value
    value = property(getv, setv, None, " value accessor : set only if best gadget")
