from .set import Set
from .write import Write
from .call import Call
from .pecall import PeCall
from .clean import Clean
from .mov import Mov
from .cpy import Cpy
from .add import Add
from .int import Int
from .strstore import StrStore
from .deref import Deref
from .retreg import RetReg
from .arithmov import ArithMov
from .pushdmov import PushdMov
from .pushpopmov import PushPopMov

name = "INTEL32"
instrs = [Set, Write, Mov, Cpy, Call, Clean, RetReg, Deref, PeCall, Add, StrStore, Int, ArithMov, PushdMov, PushPopMov]

