#!/usr/bin/python2

from ..ropinstr import RopInstr
from ropmount.disas.x86 import X86Register, X86RegSet, X86Immediat

class PeCall(RopInstr):
    """
        call a function with given args
    """
    mnemo = "pecall"
    templates = []
    # func_name,autoclean, args...
    args = (str,) + (X86Immediat,) + (X86Immediat,) * 6
    args_doc = ("function name","autoclean args ?(y/n)") + tuple("arg" + str(i) for i in range(6))
    args_min = 2

    @classmethod
    def assemble(cls, ass, args, no_regs):
        super(PeCall, cls).assemble(ass, args, no_regs)
        nb_args = len(args) - 2
        autoclean = bool(args[1].value)
        if args[0] not in ass.functions:
            raise ValueError('unknow function <{0}>'.format(args[0]))
        #Clening Arg need to preserv EAX
        no_regs.add(X86Register("ESP"))
        pop = None
        if not autoclean:
            pop = ass.raw_assemble('clean', (X86Immediat(nb_args),), no_regs | X86RegSet([X86Register('EAX')]))
            if not pop:
                return None
        #Find a valid deref/ ret
        allreg32 = set(map(X86Register, X86Register.valid32))
        allreg32 -= no_regs
        for reg in allreg32:
            deref = ass.raw_assemble("deref", (reg,), no_regs)
            ret = ass.raw_assemble("retreg", (reg,), no_regs)
            set_reg = ass.raw_assemble("set", (reg, ass.functions[args[0]]), no_regs)
            if set_reg and ret and deref:
                return PeCall(args[0], args[2:], set_reg, deref, ret, pop)

    def __init__(self, func_name, args, set_reg, deref, ret, cleaner):
        super(PeCall, self).__init__()
        self.merge(set_reg)
        self.merge(deref)
        self.merge(ret)
        for arg in args:
            self.stack.append(arg)
        if cleaner:
            self.merge(cleaner)

        self.debug = ("pecall {0} <{1}>\n"
                     "  {2}\n"
                     "  {3}\n"
                     "  {4}".format(func_name,",".join(map(str, args)), str_lvl(set_reg), str_lvl(deref), str_lvl(ret)))
        if cleaner:
            self.debug += "\n  {0}".format(str(cleaner))

    def __str__(self):
        return self.debug

def str_lvl(ropinstr):
   return str(ropinstr).replace("\n", "\n  ")
