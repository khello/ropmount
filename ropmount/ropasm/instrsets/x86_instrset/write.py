#!/usr/bin/python2

from ..ropinstr import RopInstr, ImmedOrReg, BestLen
from ropmount.disas.x86 import X86Register, X86RegSet, X86Immediat


class Write(RopInstr):
    """
        write value into an addr
        preserv src and dst
    """
    mnemo = "write"
    templates = ['mov [REG32], REG32;{,}ROP;RET']
    args = (ImmedOrReg, ImmedOrReg)
    args_doc = ("Dest", "Src")
    args_min = 2

    @classmethod
    def assemble(cls, ass, args, no_regs):
        super(Write, cls).assemble(ass, args, no_regs)
        assembles = {
                        (X86Immediat, X86Immediat) : cls.assemble_int_to_int,
                        (X86Immediat, X86Register) : cls.assemble_reg_to_int,
                        (X86Register, X86Immediat) : cls.assemble_int_to_reg,
                        (X86Register, X86Register) : cls.assemble_reg_to_reg,
                     }

        no_regs.add(X86Register('ESP'))
        args_type = tuple(map(type, args))
        return assembles[args_type](ass, args, no_regs)


    @classmethod
    def assemble_int_to_int(cls, ass, args, no_regs):
        res = {}
        for gadget in cls.gadgets[0]:
            dst_ref = gadget.instrs[0].args[0].base
            src_ref = gadget.instrs[0].args[1]
            if dst_ref in no_regs or src_ref in no_regs or no_regs & gadget.invalid_register(1):
                continue
            else:
                addr_set = ass.raw_assemble('set', (dst_ref, args[0]))
                value_set = ass.raw_assemble('set',(src_ref, args[1]))
                if addr_set and value_set:
                    w = Write(gadget, addr_set, value_set, args)
                    res[len(w.stack)] = w
        if res:
            return res[min(res)]

    #TODO : May restore args[0] if changed for write register
    @classmethod
    def assemble_int_to_reg(cls, ass, args, no_regs):
        no_regs.add(args[0])
        res = BestLen(None)
        simple_writes = ass.specialize(cls.gadgets[0], "mov [{0}], REG32;{{,}}ROP;RET".format(args[0]))
        if simple_writes:
            for gadget in simple_writes:
                value_reg = gadget.instrs[0].args[1]
                if value_reg in no_regs or gadget.invalid_register(1) & no_regs:
                    continue
                value_set = ass.raw_assemble('set', (value_reg, args[1]))
                if value_set:
                    res.value =  Write(gadget, None, value_set, args)

        #search for a high level mov
        complexe_writes = cls.gadgets[0]
        for write in complexe_writes:
            dst_reg = write.instrs[0].args[0].base
            src_reg = write.instrs[0].args[1]
            #can't use deref -> mov [REG1] , REG1
            if dst_reg == src_reg or no_regs & write.invalid_register():
                continue
            if dst_reg in no_regs or src_reg in no_regs:
                continue
            #can't write with deref [eax] if value is in eax for now
            if src_reg == args[0]:
                continue
            mov = ass.raw_assemble("cpy", (dst_reg, args[0]), no_regs)
            if not mov:
                continue
            set_value = ass.raw_assemble("set", (src_reg, args[1]), no_regs)
            if set_value:
                res.value =  Write(write, mov, set_value, args)
        return res.value

    @classmethod
    def assemble_reg_to_int(cls, ass, args, no_regs):
        #TODO : IMPLEM
        pass

    @classmethod
    def assemble_reg_to_reg(cls, ass, args, no_regs):
        #For the moment it doesn't modif src and dst
        args_noregs = X86RegSet(args) | no_regs
        simple_writes = ass.specialize(cls.gadgets[0], "mov [{0}], {1}; {{,}}ROP;RET".format(args[0], args[1]))
        for write in simple_writes:
            if not write.invalid_register(1) &  args_noregs:
                return Write(write, None, None, args)
        res = {}
        for write in ass.specialize(cls.gadgets[0], "mov [REG32], REG32; {,}ROP ;RET"):
            dst_reg = write.instrs[0].args[0].base
            src_reg = write.instrs[0].args[1]
            #remove cross-mov (need ?)
            if dst_reg == args[1] or src_reg == args[0]:
                continue
            if dst_reg == src_reg or dst_reg in no_regs or (src_reg in no_regs and not src_reg ==  args[1]):
                continue
            #respect no_registers and don't invalid args ?
            if args_noregs & write.invalid_register():
                continue

            #Need to try both sens if one mov modif the previous write register already set
            #if not dst_reg == args[1]:
            mov1 = ass.raw_assemble("cpy", (dst_reg, args[0]), no_regs)
            mov2 = ass.raw_assemble("cpy", (src_reg, args[1]), no_regs | X86RegSet([dst_reg]))
            if mov1 and mov2:
                w = Write(write, mov1, mov2, args)
                res[len(w.stack)] = w

            #if not src_reg == args[0]:
            mov2 = ass.raw_assemble("cpy", (src_reg, args[1]), no_regs)
            mov1 = ass.raw_assemble("cpy", (dst_reg, args[0]), no_regs | X86RegSet([src_reg]))
            if mov1 and mov2:
                w = Write(write, mov1, mov2, args)
                res[len(w.stack)] = w
        if res:
            return res[min(res)]

    def __init__(self, write, addr_set, value_set, args):
        super(Write, self).__init__()
        str_addr_set = str_value_set = ""
        if addr_set:
            self.stack.merge(addr_set.stack)
            str_addr_set = str(addr_set).replace("\n", "\n  ") + "\n  "
        if value_set:
            self.stack.merge(value_set.stack)
            str_value_set =  str(value_set).replace("\n", "\n  ") + "\n  "
        self.stack.append_gadget(write, [])
        self.debug = ("write [{0}], {1}".format(args[0], args[1]) + "\n  " +
                str_addr_set +
                str_value_set +
                "write {0}, {1} : <{2}>".format(write.instrs[0].args[0].base, write.instrs[0].args[1], write.descr))

    def __str__(self):
        return self.debug
