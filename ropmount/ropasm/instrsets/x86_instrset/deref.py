#!/usr/bin/python2

from ..ropinstr import RopInstr
from ropmount.disas.x86 import X86Register, X86RegSet
from ropmount.ropasm.assemblerexception import AssemblerException

class Deref(RopInstr):
    """
        dereference a register
    """
    mnemo = "deref"
    templates = ['mov REG32, [REG32];{,}ROP;ret']
    args = (X86Register,)
    args_doc = ("register to deref",)
    args_min = 1

    @classmethod
    def assemble(cls, ass, args, no_regs):
        no_regs.add(X86Register("ESP"))
        super(Deref, cls).assemble(ass, args, no_regs)
        if args[0] in no_regs:
            raise AssemblerException("Trying to deref a no_regs : {0}".format(str(args[0])))
        #General Case :
            # mov REG1, ARG0
            # mov REG2, [REG1]
            # mov ARG0, REG2
        res = {}
        for gadget in cls.gadgets[0]:
            if not gadget.invalid_register(1) & (no_regs | X86RegSet([args[0]])):
                src_reg = gadget.instrs[0].args[1].base
                dst_reg = gadget.instrs[0].args[0]
                premov = None
                postmov = None
                if src_reg != args[0]:
                    if src_reg in no_regs:
                        continue
                    premov = ass.raw_assemble("mov", (src_reg, args[0]), no_regs)
                if dst_reg != args[0]:
                    if dst_reg in no_regs:
                        continue
                    postmov = ass.raw_assemble("mov", (args[0], dst_reg), no_regs)
                if (src_reg == args[0] or premov) and (dst_reg == args[0] or postmov):
                    r = Deref(args[0], gadget, premov, postmov)
                    res[len(r.stack)] = r
        if res:
            return res[min(res)]

    def __init__(self, reg, deref, premov, postmov):
        super(Deref, self).__init__()
        self.debug = "deref {0}:\n".format(str(reg))
        if premov:
            self.merge(premov)
            self.debug += "  {0}\n".format(str(premov).replace("\n", "\n  "))
        self.debug += "  deref {0} : <{1}>".format(deref.instrs[0].args[0], deref.descr)
        self.stack.append(deref.vaddr)

        if postmov:
            self.merge(postmov)
            self.debug += "\n  {0}\n".format(str(postmov).replace("\n", "\n  "))

    def __str__(self):
        return self.debug
