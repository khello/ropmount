#!/usr/bin/python2

from ..ropinstr import RopInstr, RopReg

class Alloc(RopInstr):
    """
        Alloc some memory and set result in eax
    """
    mnemo = "alloc"
    templates = []
    args = (int,int)

    @classmethod
    def assemble(cls, ass, args, no_regs):
        super(Alloc, cls).assemble(ass, args, no_regs)

        #Clening Arg need to preserv EAX
        if not len(args):
            raise ValueError("alloc need a size")

        if len(args) == 1:
            print "Non fixed mmap :"
            return ass.raw_assemble('call', ('mmap',0,args[0],0x3,0x22,0,0), no_regs)

        print "Fixed mmap"
        return ass.raw_assemble('call', ('mmap',args[1],args[0],0x3,0x22,0,0), no_regs)

    def __init__(self, func_name, func_addr, args, cleaner):
        super(Call, self).__init__()
