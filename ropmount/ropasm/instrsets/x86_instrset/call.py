#!/usr/bin/python2

from ..ropinstr import RopInstr
from ropmount.disas.x86 import X86Register, X86Immediat
from ropmount.ropasm.assemblerexception import AssemblerException

class Call(RopInstr):
    """
        Call a function with given args
        Also clean the stack
    """
    mnemo = "call"
    templates = []
    args = (str,) + (X86Immediat,) * 6
    args_doc = ("function name",) + tuple("arg" + str(i) for i in range(6))
    args_min = 2

    @classmethod
    def assemble(cls, ass, args, no_regs):
        super(Call, cls).assemble(ass, args, no_regs)
        #Clening Arg need to preserv EAX
        no_regs.extend([X86Register('EAX')])
        nb_args = len(args) - 1
        if args[0] in ass.functions:
            addr = ass.functions[args[0]]
        else:
            #direct addr ?
            try:
                addr = int(args[0], 0)
                addr = X86Immediat(addr)
            except ValueError:
                raise AssemblerException('unknow function <{0}>'.format(args[0]))
        pop = ass.raw_assemble('clean', (X86Immediat(nb_args),), no_regs)
        if not pop:
            return None
        return Call(args[0], addr, args[1:], pop)

    def __init__(self, func_name, func_addr, args, cleaner):
        super(Call, self).__init__()
        self.name = func_name
        self.stack.append_function(func_addr, args)
        self.merge(cleaner)
        self.debug = "call {0} <{2}>\n{1}".format(func_name, str(cleaner), ",".join(map(str,args)))

    def __str__(self):
        return self.debug
