#!/usr/bin/python2

from ..ropinstr import RopInstr
from ropmount.disas.x86 import X86Immediat, X86Register

class Int(RopInstr):
    """
        Exec an interruption
    """
    mnemo = "int"
    templates = ['int CONST;ret']
    args = (X86Immediat,) * 8
    args_doc = ("int number",) + tuple("arg" + str(i) for i in range(7))
    int_reg_args = map(X86Register, ["EAX", "EBX", "ECX", "EDX", "ESI", "EDI", "EBP"])
    args_min = 1

    @classmethod
    def assemble(cls, ass, args, no_regs):
        super(Int, cls).assemble(ass, args, no_regs)
        int_nb = args[0]
        int_args = args[1:] + (-1,) * (5 - len(args))
        int_gadgets = ass.specialize(cls.gadgets[0], 'int {0};ret'.format(int_nb))
        if not int_gadgets:
            return None
        sets = []
        for reg, value in zip(cls.int_reg_args, int_args):
            if value < 0:
                continue
            set_arg = ass.raw_assemble('set', (reg, value))
            if not set_arg:
                return
            sets.append(set_arg)
        return Int(int_gadgets[0], sets, args[0])

    def __init__(self, gadget, sets, int_nb):
        super(Int, self).__init__()
        self.debug = "int {0}:".format(int_nb)
        for set_v in sets:
            self.merge(set_v)
            self.debug += "\n  "+ str(set_v)
        self.stack.append(gadget.vaddr)
        self.debug += "\n  INT"

    def __str__(self):
        return self.debug
