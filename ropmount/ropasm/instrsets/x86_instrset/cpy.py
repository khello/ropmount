#!/usr/bin/python2

from ..ropinstr import RopInstr
from ropmount.disas.x86 import X86Register, X86RegSet

class Cpy(RopInstr):
    """
        Copy a register value to another register : preserve src
    """
    mnemo = "cpy"
    templates = ['mov REG32, REG32;{,}ROP;RET']
    args = (X86Register, X86Register)
    args_doc = ("Dest", "Src")
    args_min = 2

    @classmethod
    def assemble(cls, ass, args, no_regs):
        arg1_set = X86RegSet([args[1]])
        arg0_set = X86RegSet([args[0]])
        res = ass.raw_assemble('mov', args, no_regs | arg1_set)
        if res:
            return Cpy(args, res)
        #try to find a copy to the other sens
        ret_cpy = ass.raw_assemble('mov', (args[1], args[0]), (no_regs | arg0_set) - arg1_set)
        if ret_cpy:
            #try to find a mov
            mov = ass.raw_assemble('mov', args, no_regs - arg1_set)
            if not mov:
                return
            return Cpy(args, mov, ret_cpy)

    def __init__(self, args, mov, ret=None):
        RopInstr.__init__(self)
        self.debug = []
        self.merge(mov)
        self.debug.append(str(mov).replace('\n', '\n  '))
        self.src = args[1]
        self.dst = args[0]
        if ret:
            self.merge(ret)
            self.debug.append(str(ret).replace("\n", "\n  "))
        self.debug = "cpy {0}, {1}:".format(self.dst, self.src) + "\n  " + "\n  ".join(self.debug)

    def __str__(self):
        return self.debug
