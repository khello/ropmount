#!/usr/bin/python2

from ropmount.disas.x86 import X86Register, X86Immediat
from ..ropinstr import RopInstr
from ropmount.ropasm.assemblerexception import AssemblerException

class Set(RopInstr):
    """
        Set a register to a value
    """
    mnemo = "set"
    templates = ['pop REG32; {,}ROP; ret']
    args = (X86Register, X86Immediat)
    args_doc = ("Dest", "Value")
    args_min = 2

    @classmethod
    def assemble(cls, ass, args, no_regs):
        super(Set, cls).assemble(ass, args, no_regs)
        if args[0] in no_regs:
            raise AssemblerException("Set on no_regs : {0}".format(args[0]))
        pops = ass.specialize(cls.gadgets[0], 'pop {0}; {{,}}ROP; ret'.format(str(args[0])))
        res = {}
        for pop in pops:
            if  no_regs &  pop.invalid_register():
                continue
            if args[0] in pop.invalid_register(1):
                continue
            if len(pop.instrs) == 2:
                return Set(pop, args)
            rset = Set(pop, args)
            res[len(rset.stack)] = rset
        if res:
            return res[min(res)]

    def __init__(self, set_gad, args):
        super(Set, self).__init__()
        self.stack.append_gadget(set_gad, [args[1]])
        self.debug = "set {0}, {1} : <{2}>".format(args[0], args[1], set_gad.descr)

    def __str__(self):
        return self.debug
