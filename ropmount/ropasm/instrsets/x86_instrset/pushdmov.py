#!/usr/bin/python2

from ..ropinstr import RopInstr
from ropmount.disas.x86 import X86Register
from ropmount.ropasm.assemblerexception import AssemblerException
from .mov import Mov


class PushdMov(Mov):
    """
        Mov a register to another : it doesn't preserve the source register
        Based on pushad;ret
    """
    mnemo = "pmov"
    templates = ['pushad;{,}ROP;ret', '{,}add esp, CONST; RET', 'pop REG32; add esp, CONST; ret']
    args = (X86Register, X86Register)
    args_doc = ("Dest", "Src")
    args_min = 2
    reg_order =map(X86Register, ['EDI', 'ESI', 'EBP', 'ESP', 'EBX', 'EDX','ECX', 'EAX'])


    @classmethod
    def assemble(cls, ass, args, no_regs):
        RopInstr.assemble(ass, args, no_regs)
        no_regs.add(X86Register('ESP'))
        #cls.pushad_reton(cls.gadgets[0], no_regs)
        pushad =  ass.specialize(cls.gadgets[0], "pushad;ret")
        if not pushad or X86Register('EDI') in no_regs:
            return None
        #ret_on = cls.reg_order[cls.reg_order.index(args[1]) - 1]
        cleaners = cls.clean_to(X86Register('ESI'), args[1])
        setc = None
        register_ret = None
        for c in cleaners:
            if c not in no_regs:
                register_ret = c
                clean = cleaners[c]
                setc = ass.raw_assemble('set', (X86Register('EDI'), clean.vaddr), no_regs)
                break
        if not setc:
            return None
        setter1 = (setc, clean.descr)
        #Register ret on + 1 for ret + retn value
        next_pop = cls.reg_order[cls.reg_order.index(register_ret) + 1 +
                clean.instrs[-1].args[0].value / X86Register.DEFAULT_REG_SIZE]
        size_plus, mov = cls.get_good_mov(ass, args[0], next_pop)
        if not mov:
            return None
        set_2  = ass.raw_assemble('set', (register_ret, mov.vaddr), no_regs)
        if not set_2:
            return None
        setter2 = (set_2, mov.descr)
        return PushdMov(pushad[0], setter2, setter1, size_plus, args)

    #find a gadget to : recup the entry juste after ans jump over the others
    @classmethod
    def get_good_mov(cls, ass, reg_dest, actual_reg):
        res = (0xffff, None)
        #actual register is the first register to be poped
        to_end =  len(cls.reg_order) - cls.reg_order.index(actual_reg)
        gs = ass.specialize(cls.gadgets[2], "pop {0}; add esp, CONST; ret".format(reg_dest))
        for g in gs:
            # -1 for ret
            cons = (g.stack_consumption() /X86Register.DEFAULT_REG_SIZE) - 1
            if cons < to_end or cons >= res[0]:
                continue
            res = (cons, g)
        return (res[0] - to_end, res[1])

    #WIP : handling pushad; ROP; ret 
    #@classmethod
    #def pushad_reton(cls, pushads, no_regs):
    #    res = {}
    #    for pushad in pushads:
    #        if pushad.invalid_register() & no_regs:
    #            continue
    #        pre_ret = pushad.stack_consumption() - pushad.instrs[-1].args[0].value - X86Register.DEFAULT_REG_SIZE
    #        reg = cls.get_ret_reg(X86Register('EDI'), pre_ret)

    # Clean stack just after a pushd to the given register
    # The res dict keys are the register we ret on
    @classmethod
    def clean_to(cls, first_reg, dest_reg):
        """
            return a dict that clean from first_reg to dest-reg
            first_reg is the register just after the one we ret-on
            dest_reg is the next we will ret on
        """
        diff =  cls.reg_order.index(dest_reg) - cls.reg_order.index(first_reg)
        if diff < 0:
            return []
        cleaners = cls.get_clean(diff * X86Register.DEFAULT_REG_SIZE)
        g_res = ((cls.get_ret_reg(first_reg, pre_ret), cleaners[pre_ret]) for pre_ret in cleaners)
        return dict(g_res)


    @classmethod
    def get_ret_reg(cls, first_reg, pre_ret):
        """
            Return the register we will return on given a size of stack clean
        """

        pos = cls.reg_order.index(first_reg) + (pre_ret / X86Register.DEFAULT_REG_SIZE)
        if pos < len(cls.reg_order):
            return cls.reg_order[pos]
        return None

    @classmethod
    def get_clean(cls, size):
        res = {}
        for g in cls.gadgets[1]:
            if g.stack_consumption() == size:
                 pre_ret = size - g.instrs[-1].args[0].value - X86Register.DEFAULT_REG_SIZE
                 res[pre_ret] = g
        return res

    def __init__(self, pushad, setter1, setter2, size_plus, args):
        RopInstr.__init__(self)
        self.src = args[1]
        self.dst = args[0]
        self.merge(setter1[0])
        self.merge(setter2[0])
        self.stack.append(pushad.vaddr)
        self.stack.add_garbage(size_plus * X86Register.DEFAULT_REG_SIZE)
        self.debug = ("pmov {0}, {1}:".format(*args) + "\n  " +
                str(setter1[0]).replace("\n", "\n  ") + " ({0})".format(setter1[1]) +
                "\n  "+str(setter2[0]).replace("\n", "\n  ") + " ({0})".format(setter2[1]) +
                "\n  pushad;ret\n")

    def __str__(self):
        return self.debug
