
from ..ropinstr import RopInstr, RopReg, ImmedOrReg, MultipleInstr

class JitCallException(Exception):
    pass


class JitCall(RopInstr):
    """
        Juste a JitCall Test
    """
    mnemo = "jit_call"
    templates = ['pushad;ret','{7,7}pop REG32;ret', 'mov esp, eax;ret', 'leave;ret', 'ret']

    args = (str, ImmedOrReg)

    @classmethod
    def assemble(cls, ass, args, no_regs):
        super(JitCall, cls).assemble(ass, args, no_regs)
        if args[0] not in ass.functions:
            raise ValueError('unknow function <{0}>'.format(args[0]))
        if len(args) - 1 < 1:
            raise ValueError('Call function <{0}> without args'.format(args[0]))
        alloc_noregs = []
        for arg in args:
            if arg == "EAX" or arg == "EDX":
                print "DUMMY JITCALL POC : can't use EAX or EDX"
                return None
            if type(arg) == RopReg:
                alloc_noregs.append(arg)

        #Clening Arg need to preserv EAX
        no_regs.extend([RopReg('ESP'), RopReg('EAX')])
        nb_args = len(args) - 1


        clean = ass.raw_assemble('clean', (nb_args,), no_regs + [RopReg("EBP")])


        alloc = ass.raw_assemble('alloc', (4096,), alloc_noregs)
        #EAX CONTAIN ADDR

        set_edx_midle_page = MultipleInstr()
        set_edx_midle_page.merge(alloc)


        mov_to_edx = ass.raw_assemble('mov', (RopReg("Edx"), RopReg("Eax")), no_regs + alloc_noregs)
        if not mov_to_edx:
            raise JitCallException("Cant mov eax to edx")

        add_edx_size = ass.raw_assemble('add', (RopReg("EDX"), 2047), no_regs + alloc_noregs)
        if not add_edx_size:
            raise JitCallException("Cant set edx to middle page")
        set_edx_midle_page.merge(mov_to_edx)
        set_edx_midle_page.merge(add_edx_size)

        mov_eax_edx = ass.raw_assemble('mov', (RopReg("eax"), RopReg("edx")), alloc_noregs)
        set_edx_midle_page.merge(mov_eax_edx)



        write_func_call = MultipleInstr()

        add_edx_4 = ass.raw_assemble('add', (RopReg('EDX'), 4), no_regs + alloc_noregs)
        if not add_edx_4:
            raise JitCallException("CANT ADD EDX 4")

        write_func_addr = ass.raw_assemble('write', (RopReg("Edx"), ass.functions[args[0]]), no_regs + alloc_noregs)
        write_func_clean = ass.raw_assemble('write', (RopReg("Edx"), clean.stack.values[0]), no_regs + alloc_noregs)
        if not write_func_clean or not write_func_addr:
            raise JitCallException("CANT WRITE FUNC OR CLEANER")

        write_func_call.merge(write_func_addr)
        write_func_call.merge(add_edx_4)
        write_func_call.merge(write_func_clean)
        write_func_call.merge(add_edx_4)

        for arg in args[1:]:
            write_arg =  ass.raw_assemble('write', (RopReg("Edx"), arg), no_regs + alloc_noregs)
            if not write_arg:
                raise JitCallException("Cant write arg <{0}>".format(arg))
            write_func_call.merge(write_arg)
            write_func_call.merge(add_edx_4)


        for pad in clean.stack.values[1:]:
            print "Add cleaner padding"
            write_pad =  ass.raw_assemble('write', (RopReg("Edx"), pad), no_regs)
            if not write_pad:
                raise JitCallException("Cant write pading <{0}>".format(pad))
            write_func_call.merge(write_pad)
            write_func_call.merge(add_edx_4)


        res = MultipleInstr()
        res.merge(set_edx_midle_page)
        res.merge(write_func_call)

        #FROM HERE CALL FUNCTION IS DONE
        #WRITE END OF FONCTION (RETURN)

        #GET ESP PART
        #DUMMY POC OF JITCALL
        pushad = cls.gadgets[0][0]
        pop = cls.gadgets[1][0]

        set_edi = ass.raw_assemble('set', (RopReg('EDI'), pop.addr), no_regs)
        get_esp = MultipleInstr()
        get_esp.merge(set_edi)
        get_esp.stack.append(pushad.addr)
        get_esp.debug += "\n  PUSHAD; RET"

        #res.merge(get_esp)

        write_leave = cls.write_leave_ret(ass, RopReg("EDX"))

        res.merge(write_leave)


        add_ecx_40 = ass.raw_assemble('add', (RopReg('Ecx'), 40), no_regs)

        res.merge(get_esp)
        res.merge(add_ecx_40)


        mov_ebp_ecx = ass.raw_assemble('mov', (RopReg("EBP"), RopReg("ECX")), no_regs)
        res.merge(mov_ebp_ecx)


        #For test : we have esp in ECX


        stackpivot = cls.gadgets[2][0]


        res.stack.append(stackpivot.addr)
        res.debug += "\n  STACKPIVOT"

        for i in range(40 / 4):
            res.stack.append(cls.gadgets[4][0].addr)
        res.debug += "\n  {0} * RET for slide".format(40 / 4)

        return res


    @classmethod
    def get_esp(cls):
        pass


    @classmethod
    def write_leave_ret(cls, ass, reg):
        leave_ret = cls.gadgets[3][0]
        write = ass.raw_assemble('write', (reg, leave_ret.addr))
        return write

    @classmethod
    def nb_pop_pushad(cls, pushad):
        pass

    def __init__(self, prev, set_edi, pushad):
        super(JitCall, self).__init__()

        self.debug = str(set_edi) + "\n  PUSHAD; RET"
        self.merge(set_edi)
        self.stack.append(pushad.addr)

    def __str__(self):
        return  self.debug
