#!/usr/bin/python2

import cPickle as pickle
import md5

import ropmount.disas as Disas
import ropmount.ropfinding as RopFinding
import instrsets

from .instrsets.ropinstr import ImmedOrReg, MultipleInstr
from ropmount.ropfinding.filters.intfilter import IntFilter
from .assemblerexception import AssemblerException


class RopAssembler(object):
    """
        The class that parse an instruction and assemble it
        This is also the class that mess with Filters and RopFinding
            - Search Gadget for RopInstr
            - Specialize some RopInstr gadgets
    """
    def _init_archi_params(self, archi):
        #instruction set is the instruction set of the given archi
        self.instrset = dict(((i.mnemo, i) for i in instrsets.all[archi.name].instrs))
        self.archi = archi
        self.archi_const_reg = ImmedOrReg(archi.Register, archi.Immediat)
        self.args_parse = {
                        archi.Immediat : lambda x: archi.Immediat(x),
                        archi.Register : lambda x : archi.Register(x),
                        ImmedOrReg : lambda x: self.archi_const_reg.parse(x),
                        str : lambda x : x,
                      }

    def __init__(self, mapfile):
        self.mapfile = mapfile
        self.finder = RopFinding.RopFinder()
        functions = mapfile.extractor.get_functions()
        archi = Disas.archis[mapfile.archi_name]
        self._init_archi_params(archi)
        self.functions = dict((name, archi.Immediat(value)) for name, value in functions.items())
        self.rec = 0
        self.mem = {}
        # group together all template of all instruction in the set
        all_templates = []
        for instr in self.instrset.values():
            self.mem[instr] = {}
            all_templates.extend(instr.templates)
        # Create filters for the template
        for templates in all_templates:
            self.finder.add_filter(IntFilter(templates, templates, archi))
        self.res = dict(((name, i) for name,i in zip(all_templates, self.finder.find_gadget(self.mapfile))))
        # give to each instr the result of its templates
        for instr in self.instrset.values():
            instr.gadgets = [self.res[template].values() for template in instr.templates]

    def specialize(self, gadgets, spe_template):
        """ Permit to apply re-filtering of gadget during assembling
            Exemple:
                we have a list that match : "pop REG32; ret"
                and we want to pop EAX
                We can re-filter this list:
                specialize(list, "pop EAX;ret")
        """
        spe_filter = IntFilter(spe_template, spe_template, self.archi)
        return [gadget for gadget in gadgets if spe_filter.match(gadget)]

    def assemble(self, ropprog, raise_failure=False):
        """
            Parse a string representing some instructions and send it to
            raw_assemble
        """
        instrs = ropprog.strip().split(';')
        res = []
        for instr in instrs:
            infos = instr.split('!')

            code = infos[0]
            no_regs = self.archi.RegSet()
            if len(infos) > 2:
                raise AssemblerException("More that one '!' in instr <{0}>".format(instr))
            if len(infos) == 2:
                for reg in infos[1].split(','):
                    no_regs.add(self.archi_const_reg.parse(reg.strip()))

            instr_info = code.strip().split(" ", 1)
            if instr_info[0] not in self.instrset:
                raise AssemblerException('Unknow instruction : {0}'.format(instr_info[0]))
            if len(instr_info) < 2:
                raise AssemblerException("instruction to assemble need at least one argument")
            mnemo, str_args = instr_info
            instr = self.instrset[mnemo]
            args = []
            #No list comp here to give the best message if any error
            for arg_type, arg_value in zip(instr.args, str_args.split(',')):
                try:
                    args.append(self.args_parse[arg_type](arg_value.strip()))
                except ValueError as e:
                    raise AssemblerException("Error when parsing argument {0} <{1}> : expecting type <{2}>\n"
                            "Info : {3}".format(len(args) + 1, arg_value, arg_type.__name__, e))

            i = self.raw_assemble(mnemo, tuple(args), no_regs)
            if not i:
                if raise_failure:
                    raise AssemblerException("Can't assemble : <{0}>".format(code))
                return None
            res.append(i)

        ret = MultipleInstr()
        for i in res:
            ret.merge(i)
        return ret

    def raw_assemble(self, mnemo, args, no_regs=None):
        """
            Assemble a single instr:
            Used by
                    - RopAssembler.assemble
                    - RopInstr
        """
        if no_regs is None:
            no_regs = self.archi.RegSet()
        self.rec += 1
        if mnemo not in self.instrset:
            raise AssemblerException("Unknow instr : <{0}>".format(mnemo))

        instr = self.instrset[mnemo]
        for arg, expect_type in zip(args, instr.args):
            if not isinstance(arg, expect_type):
                raise AssemblerException("Bad type Instr {0} : {1} expected ".format(arg.__class__, expect_type))
        found, res = self.get(instr, args, no_regs)
        if not found:
            res = instr.assemble(self, args, self.archi.RegSet(no_regs)) #Deep copy of no_regs
            self.save(instr, args, no_regs, res)
        self.rec -= 1
        return res

    def get(self, instrcls, args, no_regs):
        """
            Restore already assembled instr depending on args and no_regs
            return tuple : (Found, Instr)
        """
        instrmem = self.mem[instrcls]
        if args not in instrmem:
            return (False, None)
        all_res = [(self.consum(i), i, noreg) for noreg,i in instrmem[args] if no_regs <= noreg]
        if not all_res:
            return (False, None)
        #If something is found:
        best = min(all_res)
        if  best[1]:
            return (True, min(all_res)[1])
        if not best[2] ==  no_regs:
            return (False, None)
        return (True,None)

    def save(self, instrcls, args, no_regs, result):
        """
            Save a assembled instruction with its args and no_regs
        """
        instrmem = self.mem[instrcls]
        if args not in instrmem:
            instrmem[args] = [(no_regs, result)]
            return
        instrmem[args] = [(noreg, ins) for noreg, ins in instrmem[args] if
                not noreg <= no_regs or self.consum(ins) < self.consum(result)]
        instrmem[args].append((no_regs, result))

    def consum(self, x):
        """
            return the stack consumption of x
            or a big value if None
        """
        if not x:
            return 0xffffffff
        return len(x.stack)


class DumpRopAssembler(RopAssembler):
    """
        A RopAssembler that dump found gadget in a file
        to retrieve them fastet at the next use of the binary file
    """
    def __init__(self, mapfile, filename):
        self.mapfile = mapfile
        self.finder = RopFinding.RopFinder()
        archi = Disas.archis[mapfile.archi_name]
        self._init_archi_params(archi)
        functions = mapfile.extractor.get_functions()
        self.functions = dict((name, archi.Immediat(value)) for name, value in functions.items())
        self.rec = 0
        self.mem = {}
        for instr in self.instrset.values():
            self.mem[instr] = {}
        try:
            f = open(filename, "rb")
        except IOError:
            self.loadfail(mapfile, filename, "No dump file")
            return
        try:
            if not self.check_mapfile_restore(f, mapfile):
                f.close()
                self.loadfail(mapfile, filename, "Bad md5")
                return
            res = pickle.load(f)
        except EOFError:
            f.close()
            self.loadfail(mapfile, filename, "pickle fail")
            return
        for instr in self.instrset.values():
            try:
                instr.gadgets = [res[template].values() for template in instr.templates]
                f.close()
            except KeyError:
                f.close()
                self.loadfail(mapfile, filename, "missing information")

    def check_mapfile_restore(self, restore_file, mapfile):
        fmd5 = pickle.load(restore_file)
        if not isinstance(fmd5, str):
            return False
        return fmd5 == self.calc_mapfile_md5(mapfile)

    def calc_mapfile_md5(self, mapfile):
        mapfile.f.seek(0,0)
        data = mapfile.f.read()
        return md5.md5(data).digest().encode('hex')

    def loadfail(self, mapfile, filename, msg):
        print "Failed to retrieve information from dumpfile : ", msg
        print "Analysing exe file"
        super(DumpRopAssembler, self).__init__(mapfile)
        print "dumping result to dumpfile"
        f = open(filename, "wb")
        pickle.dump(self.calc_mapfile_md5(mapfile), f, -1)
        pickle.dump(self.res, f, -1)
        f.close()
        print "Done !"
        print
