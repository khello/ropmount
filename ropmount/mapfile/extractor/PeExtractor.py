#! /usr/bin/python2

import ropmount.utils.depack as depack
from extractor import Extractor

#constant

IMAGE_SCN_CNT_CODE = 0x00000020

#type
BYTE = "B"
WORD = "H"
DWORD = "I"
PDWORD = DWORD
LONG = "L"

#struct

DOS_HEADER = [
        ("e_magic", WORD),
        ("padind", depack.padding(WORD * 29)),
        ("e_lfanew", LONG)]

FILE_HEADER = [
        ("Machine", WORD),
        ("NbOfSections", WORD),
        ("padding", depack.padding(DWORD * 3 + 2 * WORD))]

DATA_DIRECTORY = [
        ('VirtualAddress', DWORD),
        ('Size', DWORD)]


OPTIONAL_HEADER = [
        ("Magic", WORD),
        ("padding", depack.padding(2 * BYTE)),
        ("padding", depack.padding(4 * DWORD)),
        ("BaseOfCode", DWORD),
        ("BaseOfData", DWORD),
        ("ImageBase", DWORD),
        ("padding", depack.padding(2 * DWORD + 6 * WORD + 4 * DWORD + 2 * WORD + 5 * DWORD)),
        ("NumberOfRvaAndSizes", DWORD),
        ("DataDirectory", (DATA_DIRECTORY,) * 16)]

NT_HEADER = [
        ("Signature", DWORD),
        ("FileHeader", FILE_HEADER),
        ("OptionalHeader", OPTIONAL_HEADER)]


SECTION_HEADER = [
        ("Name", BYTE * 8),
        ("VirtualSize", DWORD),
        ("VirtualAddress", DWORD),
        ("SizeOfRawData", DWORD),
        ("PtrToRawData", DWORD),
        ("padding", depack.padding(2 * DWORD + 2 * WORD)),
        ("Characteristics", DWORD)]


IMPORT_TABLE_POS = 1

IMPORT_DESCRIPTOR = [
        ("OriginalFirstThunk", DWORD),
        ("TimeDateStamp", DWORD),
        ("ForwarderChain", DWORD),
        ("Name", DWORD),
        ("FirstThunk", DWORD)]

THUNK_DATA = [
        #("Function", PDWORD),
        ("AddressOfData", PDWORD)]

IMPORT_BY_NAME = [
        ("Hint", WORD),
        ("Name", BYTE)]


class PeExtractor(Extractor):
    def __init__(self, mapfile):
        self.mapfile = mapfile

    def get_dll_name(self, imp_descr, diff):
        f = self.mapfile.f
        pos = f.tell()
        f.seek(imp_descr['Name'] - diff)
        name = f.read(50).split('\x00')[0]
        f.seek(pos)
        return name

    def extract_thunk_name(self, thunk, diff):
        f = self.mapfile.f
        pos = f.tell()
        f.seek(thunk["AddressOfData"] - diff)
        name = f.read(51)[2:].split('\x00')[0]
        f.seek(pos)
        return name


    def get_offset_section(self, addr):
        for section in self.sections:
            if section["VirtualAddress"] <= addr <=  section["VirtualAddress"] + section["VirtualSize"]:
                return section["VirtualAddress"] - section["PtrToRawData"]
        raise Exception('OUPS')

    def get_iat_functions(self):
        f = self.mapfile.f
        f.seek(0,0)
        res = dict()
        DosHeader = depack.depack(DOS_HEADER, f)
        f.seek(DosHeader['e_lfanew'],0)
        PeHeader = depack.depack(NT_HEADER, f)
        Base = PeHeader['OptionalHeader']['ImageBase']
        self.sections = []
        for _ in range(PeHeader['FileHeader']['NbOfSections']):
            section = depack.depack(SECTION_HEADER, f)
            self.sections.append(section)

        TST_OFFSET =   self.get_offset_section(PeHeader["OptionalHeader"]["DataDirectory"][IMPORT_TABLE_POS]['VirtualAddress'])

        f.seek(PeHeader["OptionalHeader"]["DataDirectory"][IMPORT_TABLE_POS]['VirtualAddress'] - TST_OFFSET)

        res = {}

        while True:
            imp_descr = depack.depack(IMPORT_DESCRIPTOR, f) # DEBUG
            if imp_descr['Name'] == 0:
                break
            IAT = imp_descr["FirstThunk"]
            INT = imp_descr["OriginalFirstThunk"]
            pos = f.tell()
            f.seek(IAT - TST_OFFSET)
            while True:
                addr = f.tell() + TST_OFFSET + Base
                thunk = depack.depack(THUNK_DATA, f)
                if thunk["AddressOfData"] == 0:
                    break
                res[self.extract_thunk_name(thunk, TST_OFFSET)] = addr
            f.seek(pos)
        return res

    def get_functions(self):
        return self.get_iat_functions()
