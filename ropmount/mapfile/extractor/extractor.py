#!/usr/bin/python2

class Extractor(object):
    """A class that extract function name and adresses from a file """
    def __init__(self, mapfile):
        self.mapfile = mapfile

    def get_functions(self):
        """Return all functions  in the mapfile
           format : {symbole_name : vaddr}"""
