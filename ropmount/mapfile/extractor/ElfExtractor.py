#!/usr/bin/python2

#POC class for extracting function

import ropmount.utils.depack as depack
#import ropmount.disas.x86
from ropmount.disas.x86 import X86DisasDB, X86MemAccess
from .extractor import Extractor

#Constant

ELFCLASSNONE = 0x0
ELFCLASS32 = 0x1
PF_X = 0x1

#Type

T32ADDR = "I"
T32HALF = "H"
T32OFF = "I"
T32SWORD = "i"
T32WORD = "I"
TUchar = "B"

#Struct

elf32_Ehdr = [
        ("e_ident", "I" + "x" * 12), #Others bytes doesn't matter : Discard
        ("e_type", T32HALF),
        ("e_machine", T32HALF),
        ("e_version", T32WORD),
        ("e_entry", T32ADDR),
        ("e_phoff", T32OFF),
        ("e_shoff", T32OFF),
        ("e_flags", T32WORD),
        ("e_ehsize", T32HALF),
        ("e_phentsize", T32HALF),
        ("e_phnum", T32HALF),
        ("e_shentsize", T32HALF),
        ("e_shnum", T32HALF),
        ("e_shstrndx", T32HALF)]


elf32_Shdr = [
        ("sh_name", T32WORD),
        ("sh_type", T32WORD),
        ("sh_flags", T32WORD),
        ("sh_addr", T32ADDR),
        ("sh_offset", T32OFF),
        ("sh_size", T32WORD),
        ("sh_link", T32WORD),
        ("sh_info", T32WORD),
        ("sh_addralign", T32WORD),
        ("sh_entsize", T32WORD)]

elf32_Sym = [
        ("st_name", T32WORD),
        ("st_value", T32ADDR),
        ("st_size", T32WORD),
        ("st_info", TUchar),
        ("st_other", TUchar),
        ("st_shndx", T32HALF)]

elf32_Rel = [
        ("r_offset",T32ADDR),
        ("r_info",T32WORD)]

elf32_Rela = [
        ("r_offset",T32ADDR),
        ("r_info",T32WORD),
        ("r_addend",T32SWORD)]



class Elf32Extractor(Extractor):

    def get_section(self, f, idx):
        save_pos = f.tell()
        shdr = self.get_section_header(f, idx)
        f.seek(shdr['sh_offset'])
        data = f.read(shdr['sh_size'])
        f.seek(save_pos)
        return data

    def get_section_header(self, f, idx):
        save_pos = f.tell()
        f.seek(self.Ehdr['e_shoff'] + self.Ehdr['e_shentsize'] * idx)
        shdr = depack.depack(elf32_Shdr, f)
        f.seek(save_pos)
        return shdr

    def extract_section(self, f, shdr):
        save_pos = f.tell()
        f.seek(shdr['sh_offset'])
        data = f.read(shdr['sh_size'])
        f.seek(save_pos)
        return data

    def get_str(self, offset, str_section):
        return str_section[offset:str_section.find("\x00", offset)]

    def get_syms_from_symtab(self, f ,ShdrSymTab, StrSymTab):
        """Extract all syms from symtab with a name and an addr"""
        res = {}
        f.seek(ShdrSymTab['sh_offset'])
        for _ in range(ShdrSymTab['sh_size'] / ShdrSymTab['sh_entsize']):
            Sym = depack.depack(elf32_Sym, f)
            if Sym['st_name'] and Sym['st_value']:
                res[self.get_str(Sym['st_name'], StrSymTab)] = Sym['st_value']
        return res

    def get_function_from_plt(self, f, ShdrPlt, ShdrRelPlt, ShdrDynSym, StrSym):
        #extract all dyn Symbols
        res = {}
        Syms = []
        f.seek(ShdrDynSym['sh_offset'])
        #Possible for static linked file
        if not ShdrDynSym['sh_entsize'] or not ShdrRelPlt['sh_entsize']:
            return []
        for _ in range(ShdrDynSym['sh_size'] / ShdrDynSym['sh_entsize']):
            Sym = depack.depack(elf32_Sym, f)
            Syms.append(Sym)
        #extract plt relocation {offset -> name}
        offset_to_name = {}
        f.seek(ShdrRelPlt['sh_offset'])
        for i in range(ShdrRelPlt['sh_size'] / ShdrRelPlt['sh_entsize']):
            Rel = depack.depack(elf32_Rel, f)
            pos = Rel['r_info'] >> 8
            offset_to_name[Rel['r_offset']] = self.get_str(Syms[pos]['st_name'] , StrSym)
        #Disas plt and recup dyn fonction offset and name
        Plt = self.extract_section(f, ShdrPlt)
        plt_base_addr = ShdrPlt['sh_addr']
        DB_instr = X86DisasDB(Plt, 0)
        disas_index = 0
        while disas_index < len(Plt):
            instr = DB_instr.disas(disas_index)
            if not instr:
                disas_index += 1
                break
            if instr.mnemo == 'jmp' and isinstance(instr.args[0], X86MemAccess):
                if instr.args[0].displacement in offset_to_name:
                    res[offset_to_name[instr.args[0].displacement]] = disas_index + plt_base_addr
            disas_index += instr.size
        return res

    def get_symbols(self):
        f = self.mapfile.f
        f.seek(0,0)
        Ehdr = depack.depack(elf32_Ehdr, f)
        self.Ehdr = Ehdr
        StrSection = self.get_section(f, Ehdr['e_shstrndx'])
        f.seek(Ehdr['e_shoff'])
        ShdrSymTab = None
        StrSymTab = None
        ShdrRelPlt = None
        for _ in range(Ehdr['e_shnum']):
            Shdr = depack.depack(elf32_Shdr, f)
            if self.get_str(Shdr['sh_name'], StrSection) == ".symtab":
                ShdrSymTab = Shdr
                StrSymTab = self.get_section(f, Shdr['sh_link'])
            if self.get_str(Shdr['sh_name'], StrSection) == ".rel.plt":
                ShdrRelPlt = Shdr
                ShdrDynSym = self.get_section_header(f, Shdr['sh_link'])
                StrSym = self.get_section(f, ShdrDynSym['sh_link'])
            if self.get_str(Shdr['sh_name'], StrSection) == ".plt":
                ShdrPlt = Shdr
        functions  = {}
        if ShdrSymTab:
            res = self.get_syms_from_symtab(f, ShdrSymTab, StrSymTab)
            functions.update(res)
        if not ShdrRelPlt or not ShdrPlt:
            print "No .plt or .rel.plt section : can't extract dyn loader functions"
            return functions
        res = self.get_function_from_plt(f, ShdrPlt, ShdrRelPlt, ShdrDynSym, StrSym)
        functions.update(res)
        return functions

    def get_functions(self):
        """Return all symbols/ functions that are in exec segment"""
        syms = self.get_symbols()
        mapping = self.mapfile.get_mapping()
        in_mapping = lambda addr : any([low <= addr < high for low, high in mapping])
        function_gen = ((name, addr) for name,addr in syms.items()
                            if in_mapping(addr))
        return dict(function_gen)
