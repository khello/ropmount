#! /usr/bin/python2

import ropmount.utils.depack as depack
from .mapfile import MapFile
from .extractor.ElfExtractor import Elf32Extractor

#Constant

ELFCLASSNONE = 0x0
ELFCLASS32 = 0x1
PF_X = 0x1

EM_386 = 0x3

#Type

T32ADDR = "I"
T32HALF = "H"
T32OFF = "I"
T32SWORD = "i"
T32WORD = "I"
TUchar = "B"

#Struct

elf32_Ehdr = [
        ("e_ident", "I" + "x" * 12), #Others bytes doesn't matter : Discard
        ("e_type", T32HALF),
        ("e_machine", T32HALF),
        ("e_version", T32WORD),
        ("e_entry", T32ADDR),
        ("e_phoff", T32OFF),
        ("e_shoff", T32OFF),
        ("e_flags", T32WORD),
        ("e_ehsize", T32HALF),
        ("e_phentsize", T32HALF),
        ("e_phnum", T32HALF),
        ("e_shentsize", T32HALF),
        ("e_shnum", T32HALF),
        ("e_shstrndx", T32HALF)]

elf32_Phdr = [
        ("p_type", T32WORD),
        ("p_offset", T32OFF),
        ("p_vaddr", T32ADDR),
        ("p_paddr", T32ADDR),
        ("p_filesz", T32WORD),
        ("p_memsz", T32WORD),
        ("p_flags", T32WORD),
        ("p_align", T32WORD)]

#Class

class Elf32MapFile(MapFile):
    def __init__(self, f):
        super(Elf32MapFile, self).__init__(f)
        self.__mapping = self.elf32_map(self.f)
        self.extractor = Elf32Extractor(self)

    def elf32_map(self, f):
        res = []
        f.seek(0,0)
        Ehdr = depack.depack(elf32_Ehdr, f)
        #GET Archi
        if Ehdr['e_machine'] != EM_386:
            raise NotImplementedError("Elf32 : Not Intel Archi")
        self.archi_name = "INTEL32"
        f.seek(Ehdr['e_phoff'])
        for _ in range(Ehdr['e_phnum']):
            Phdr = depack.depack(elf32_Phdr, f)
            if (Phdr['p_flags'] & PF_X):
                res.append(Phdr)
        return res

    def offset_from_vaddr(self, addr):
        for m in self.__mapping:
            if m['p_vaddr'] <= addr <=  m['p_vaddr'] + m['p_memsz']:
                offset = addr - m['p_vaddr'] + m['p_offset']
                return offset

    def get_mapping(self):
        res = []
        for m in self.__mapping:
            res.append((m['p_vaddr'], m['p_vaddr'] + m['p_memsz']))
        return res

# Functions

def is_elf(f):
    f.seek(0,0)
    begin = f.read(4)
    return begin == "\x7fELF"

def is_32bit(f):
    f.seek(4,0)
    t = f.read(1)
    return ord(t) == ELFCLASS32

def is_elf32(f):
    return is_elf(f) & is_32bit(f)
