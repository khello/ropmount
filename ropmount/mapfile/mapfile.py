#! /usr/bin/python2

class MapFile(object):
    """
        Class For File mapping and data restoring from virtual addresses
    """
    def __init__(self, file):
        self.f = file
        self.__mapping = []
        self.archi_name = ""

    def offset_from_vaddr(self, vaddr):
        """ Return offset in file of virtual address vaddr """
        raise NotImplementedError

    def get_mapping(self):
        """
            Return list of tupple representing valid code segment addresses
            form is (begin, end) for each segment
        """
        raise NotImplementedError

    def read_from_vaddr(self, vaddr, size):
        offset = self.offset_from_vaddr(vaddr)
        self.f.seek(offset)
        return self.f.read(size)
