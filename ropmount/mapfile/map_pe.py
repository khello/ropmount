#! /usr/bin/python2

import ropmount.utils.depack as depack
from .mapfile import MapFile
from .extractor.PeExtractor import PeExtractor


#constant

IMAGE_SCN_CNT_CODE = 0x00000020
IMAGE_FILE_MACHINE_I386 = 0x014c

#type
BYTE = "B"
WORD = "H"
DWORD = "I"
LONG = "L"

#struct

DOS_HEADER = [
        ("e_magic", WORD),
        ("padind", depack.padding(WORD * 29)),
        ("e_lfanew", LONG)]

FILE_HEADER = [
        ("Machine", WORD),
        ("NbOfSections", WORD),
        ("padding", depack.padding(DWORD * 3 + 2 * WORD))]

OPTIONAL_HEADER = [
        ("Magic", WORD),
        ("padding", depack.padding(2 * BYTE)),
        ("padding", depack.padding(6 * DWORD)),
        ("ImageBase", DWORD),
        ("padding", depack.padding(2 * DWORD + 6 * WORD + 4 * DWORD + 2 * WORD + 6 * DWORD)),
        ("padding_DATADIR", depack.padding(2 * DWORD * 16))]

NT_HEADER = [
        ("Signature", DWORD),
        ("FileHeader", FILE_HEADER),
        ("OptionalHeader", OPTIONAL_HEADER)]


SECTION_HEADER = [
        ("Name", BYTE * 8),
        ("VirtualSize", DWORD),
        ("VirtualAddress", DWORD),
        ("SizeOfRawData", DWORD),
        ("PtrToRawData", DWORD),
        ("padding", depack.padding(2 * DWORD + 2 * WORD)),
        ("Characteristics", DWORD)]

class PeMapFile(MapFile):

    def __init__(self, f):
        super(PeMapFile, self).__init__(f)
        self.filetype = "Windows PE"
        self.__mapping = self.pe_map(self.f)
        self.extractor = PeExtractor(self)

    def pe_map(self, f):
        f.seek(0,0)
        res = dict()
        DosHeader = depack.depack(DOS_HEADER, f)
        f.seek(DosHeader['e_lfanew'],0)
        PeHeader = depack.depack(NT_HEADER, f)
        #Check Archi
        if not PeHeader['FileHeader']['Machine'] == IMAGE_FILE_MACHINE_I386:
            raise NotImplementedError("Pe: NotImplemented Machine Format")
        self.archi_name = "INTEL32"
        res.update({"Base" : PeHeader['OptionalHeader']['ImageBase']})
        res.update({"Sections": []})
        for _ in range(PeHeader['FileHeader']['NbOfSections']):
            section = depack.depack(SECTION_HEADER, f)
            if section['Characteristics'] & IMAGE_SCN_CNT_CODE:
                res['Sections'].append(section)
        return res

    def offset_from_vaddr(self, addr):
        base = self.__mapping['Base']
        for m in self.__mapping['Sections']:
            if base + m['VirtualAddress'] <= addr <=  base + m['VirtualAddress'] + m['VirtualSize']:
                offset = addr - base - m['VirtualAddress'] + m['PtrToRawData']
                return offset

    def get_mapping(self):
        res = []
        base = self.__mapping['Base']
        for m in self.__mapping['Sections']:
            res.append((base + m['VirtualAddress'], base + m['VirtualAddress'] + m['VirtualSize']))
        return res



def is_pe(f):
    f.seek(0,0)
    begin = f.read(2)
    return begin == "MZ"

