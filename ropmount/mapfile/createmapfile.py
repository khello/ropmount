#! /usr/bin/python2

import map_elf32
import map_pe

# The list of type supported, each the tuple contains:
#  - The function to verify is the file is of Format F (param = file)
#  - The class corresponding to this format (param = file)

ExeFormat = [
        (map_elf32.is_elf32, map_elf32.Elf32MapFile ),
        (map_pe.is_pe, map_pe.PeMapFile)
        ]

class FormatNotImplemented(Exception):
    pass

def create_mapfile(filename):
    """ Return a MapFile if type of filename is handled"""
    f = open(filename, "rb")
    for verif, MapClass in ExeFormat:
        if (verif(f)):
            return MapClass(f)
    raise FormatNotImplemented("Unknow format for file :" + filename)
