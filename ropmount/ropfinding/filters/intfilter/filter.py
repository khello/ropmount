#!/usr/bin/python2

from ropmount.ropfinding.filters.ropfilter import RopFilter
import asmparser

class IntFilter(RopFilter):
    """
        A generic filter based on an ASM-like syntax
    """
    #ret and retn
    interest_op = ['\xc3', '\xc2']
    MAX_INSTR_SIZE = 15

    def __init__(self, template, name, archi):
        super(IntFilter, self).__init__()
        asmparser.Archi = archi
        g = asmparser.GadgetMatcher.parse(template)
        self.gadget = g
        self.max_size = g.size * IntFilter.MAX_INSTR_SIZE
        self.name = name
        self.i = 0

    #need to be more smart about when stop
    def filter(self, validator):
        #local variable for speed
        self_graph_match = self.gadget.graph.rec_match
        graph_entries = self.gadget.graph.entries
        self_max_size =  self.max_size
        self_filtered = self.filtered
        gadget = iter(validator).next()
        instr = iter(gadget).next()
        if instr.mnemo[0:3] != 'ret':
            raise ValueError('Bad instruction :'
                    + instr.mnemo)

        for gadgetdisas in validator:
                # This function is heavely called
                # wrappers are slow : faster to directly call grahp.rec_match
            if self_graph_match((gadgetdisas.instrs, 0), graph_entries):
                self_filtered[gadgetdisas.vaddr] = gadgetdisas
            else:
                if validator.base_vaddr - gadgetdisas.vaddr.value > self_max_size:
                    break
        return

    def match(self, gadget):
        return self.gadget.graph.rec_match((gadget.instrs, 0), self.gadget.graph.entries)
