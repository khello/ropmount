#!/usr/bin/python2

class RopFilter(object):
    """
        A filter choose GadgetDisas it considers interesting from a
        GadgetValidator and register them
    """
    # the opcodes that interest the filter
    interest_op = []

    def __init__(self):
        self.filtered = dict()

    def filter(self, validator):
        """
            This function extract interesting gadget
            from a GadgetValidator
        """
        raise NotImplementedError("RopFilter main class isn't a valid filter")

    def get_filtered(self):
        """
            Post processing function : call when filtering is complete
        """
        return self.filtered

    def clear_result(self):
        """
            Reset the result in order to reuse the filter
        """
        self.filtered = dict()

