#! /usr/bin/python2

from ropmount.ropfinding.opcodefinder import OpcodeFinder
from ropmount.ropfinding.filters.ropfilter import RopFilter

class RopFinder(object):
    """
        The class that apply filters on a mapfile
    """

    def __init__(self):
        self.filters = []

    def add_filter(self, filt):
        if not isinstance(filt, RopFilter):
            raise ValueError("Invalid filter : filters must inherit from RopFilter class")
        self.filters.append(filt)

    def find_gadget(self, mapfile):
        """
            Returns a list of dict with filtered gadgets in form :
                {name : Gadget}
        """
        opfinder = OpcodeFinder(mapfile)
        for f in self.filters:
            opfinder.rop_opcodes += f.interest_op
            f.clear_result()
        #Opcode finder will stop on every opcode that interest each filter
        opfinder.rop_opcodes = list(set(opfinder.rop_opcodes))
        for cur_opcode, validator in opfinder:
            # For each interesting opcode we search which filters are interested
            for f in self.filters:
                if cur_opcode in f.interest_op:
                    f.filter(validator)
        result = []
        for f in self.filters:
            filtered = f.get_filtered()
            result.append(filtered)
        return result
