#!/usr/bin/python2

from ropmount.disas.gadget import GadgetDisas
import ropmount.disas as Disas

class GadgetValidator(object):
    """
        This is the object that search valid gadgets
        backward from a begin instruction
    """
    MAX_FAIL = 16

    def __init__(self, block_DB, base_vaddr):
        self.block_DB = block_DB
        self.base_vaddr = base_vaddr
        self.mem = {}
        last_instr = block_DB.disas(base_vaddr)
        if not last_instr:
            raise ValueError("[validator] : Can't assemble last instr")
        self.mem[0] = GadgetDisas([last_instr])

    def __iter__(self):
        """
            Return valid GadGetDisas with last instruction at index base_vaddr
        """
        index = 0
        fails = 0
        min_index = self.block_DB.vaddr - self.base_vaddr
        while fails != self.MAX_FAIL and index >= min_index:
            if index in self.mem:
                gadget =  self.mem[index]
            else:
                gadget = self.calc_GadgetDisas(index)
                self.mem[index] = gadget
            fails += 1
            if gadget:
                fails = 0
                yield gadget
            index -= 1

    def calc_GadgetDisas(self, index):
        """
           Disas instruction at index
           Check that the instr + others lead to the last instruction
        """
        instrdisas = self.block_DB.disas(self.base_vaddr + index)
        if not instrdisas:
            return None
        index += instrdisas.size
        next_gadget = self.mem.get(index, None)
        #No gadget or gadget overflow on last_instr
        if index > 0 or not next_gadget:
            return None
        return GadgetDisas([instrdisas] + next_gadget.instrs)


class OpcodeFinder(object):
    """
        The class that search opcode from a mapfile:
        Will stop on every opcode in self.rop_opcodes list
    """

    def __init__(self, mapfile):
        self.mapfile = mapfile
        self.archi = Disas.archis[mapfile.archi_name]
        #The opcodes we will stop on
        self.rop_opcodes  = []

    def __iter__(self):
        """ Iter over all rop_opcodes on all mapfile segments
            return (opcode, validator)
        """
        for vaddr, block in self.code_block_iterator():
            # We use ONE DisasDB per block to gain time
            # Create a DisasDB could take time (lot of call if one per validator)
            # Validator may redisas same instr that older validator : memoization
            block_DB = self.archi.DisasDB(block, vaddr)
            for opcode, index in self.opcode_iterator(block):
                yield (opcode, GadgetValidator(block_DB, index + vaddr))

    def code_block_iterator(self):
        """
            Iter over mapfile segments
            Return (vaddr, segment_code)
        """
        for vaddr, vaddr_end in self.mapfile.get_mapping():
            block = self.mapfile.read_from_vaddr(vaddr,
                    vaddr_end - vaddr)
            yield (vaddr, block)

    def opcode_iterator(self, block):
        """
            Iter over all opcodes on a given segment
            return (opcode, index_in_segment)
        """
        for opcode in self.rop_opcodes:
            last_index = -1
            while True:
                next_index = block.find(opcode, last_index + 1)
                if next_index == -1:
                    break
                last_index = next_index
                yield (opcode, next_index)
