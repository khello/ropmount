#! /usr/bin/python2

from ropmount.ropcontext import RopContext
from ropmount.ropfinding.filters.intfilter import IntFilter

Predef_template = {
'stackpivot' : ['mov esp, [REG32]; {,}ROP; RET',
                'mov esp,[REG32 + CONST]; {,}ROP; RET',
                'mov esp, REG32 ; {,}ROP; RET',
                'xchg esp, REG32; {,}ROP; RET',
                'xchg REG32, esp; {,}ROP; RET',
                'push REG32;{,}ROP ;pop esp; {,}ROP; RET',
                'push REG32; {,}ROP; {1,}pop REG32;{,}ROP;RET',
                'add esp, REG32; {,}ROP; RET',
                'add esp, [REG32 + CONST]; {,}ROP; RET',
                'add esp, CONST; {,}ROP; RET',
                'sub esp, REG32; {,}ROP; RET',
                'sub esp, [REG32 + CONST]; {,}ROP; RET',
                'sub esp, CONST; {,}ROP; RET',
                'xor esp, REG32; {,}ROP; RET',
                'xor esp, [REG32 + CONST]; {,}ROP; RET',],

'pops' :   ['{1,}pop REG32; RET',],
'pushes' : ['{1,}push REG32; RET',],
'read' :   ['mov REG32, [REG32]; RET'],
'write' :  ['mov [REG32], REG32; RET'],
'pushad':  ['pushad; {,}add esp,CONST; RET'],

'regs' :   ['inc REG32;{,}pop REG32;RET',
            'dec REG32;{,}pop REG32;RET',
            'mov REG32, REG32;{,}pop REG32;RET',],

'get_esp' : ['mov REG32, esp; {,}pop REG32;RET',
             'add REG32, esp; {,}pop REG32;RET',
             'sub REG32, esp; {,}pop REG32;RET',
             'xor REG32, esp; {,}pop REG32;RET',
             'or REG32, esp; {,}pop REG32;RET',
             'and REG32, esp; {,}pop REG32;RET',
             'lea REG32, [esp]; {,}pop REG32;RET',
             'lea REG32, [esp + CONST]; {,}pop REG32;RET',
             'mov [esp], esp; {1,}pop REG32;RET',
             'mov [REG32], esp; {,}pop REG32;RET',],
                  }



def Entry(filename, templates):
    """entry point of the dumper front-end"""
    rpc = RopContext(filename)
    names = []

    if 'all' in templates:
        templates.remove('all')
        templates += Predef_template

    for i, template in enumerate(templates):
        if template.strip() in Predef_template:
            for j, templ_decl in enumerate(Predef_template[template.strip()]):
                rpc.finder.add_filter(IntFilter(templ_decl, templ_decl + str(j), rpc.archi))
                names.append(templ_decl)
        else:
            rpc.finder.add_filter(IntFilter(template, 'filter' + str(i), rpc.archi))
            names.append(template)

    for i, dg in enumerate(rpc.extract_gagdets()):
        print '---'
        print names[i]
        if not len(dg):
            print "[] None"
            continue
        for item_name in dg:
            ropitem = dg[item_name]
            descr = "[{0}] {1}".format(ropitem.vaddr, ropitem.descr)
            print descr
