#! /usr/bin/python2

import ropmount.mapfile as MapFile
import ropmount.ropfinding as RopFinding
import ropmount.disas as Archi


class RopContext:
    """
        Contain a complete Context.
        This the class that frontends use
    """
    def __init__(self, filename):
        self.mapfile = MapFile.create_mapfile(filename)
        self.finder = RopFinding.RopFinder()
        self.archi = Archi.archis[self.mapfile.archi_name]

    def extract_gagdets(self):
        dict_list_gadget = self.finder.find_gadget(self.mapfile)
        return dict_list_gadget
