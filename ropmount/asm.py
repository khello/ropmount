#! /usr/bin/python2
"""A RopAssembler front-end based on a simple shell"""
import readline
import struct
import traceback

from ropmount.ropcontext import RopContext
from ropmount.ropasm.assembler import DumpRopAssembler
from ropmount.ropasm.assemblerexception import AssemblerException
from ropmount.disas.type import Register, Immediat

def Entry(filename):
    """Entry point of this front-end"""
    rpc = RopContext(filename)
    ass = DumpRopAssembler(rpc.mapfile, filename + '.rdp')
    instr = None

    print "simple assembler shell"
    print "options are:"
    for i in Options:
        print "    {0} : {1}".format(i, Options[i].__doc__)
    while True:
        try:
            cmd = raw_input(">>> ")
        except EOFError:
            break
        try:
            if cmd == "quit" or cmd == "exit":
                break
            if cmd in Options:
                Options[cmd](instr, ass)
                continue
            instr = ass.assemble(cmd, raise_failure=False)
            if instr:
                print instr
            else:
                print "Can't assemble this"
        except AssemblerException as e:
            print traceback.format_exc()
            print "Error :",
            print str(e)
            continue



def print_stack(ropinstr, assembler):
    """Print the stack of the last assembled gadget"""
    if not ropinstr:
        print "no stack to print"
        return
    garbage_size = 0
    for immed in ropinstr.stack.values:
        if immed.size == 1:
            garbage_size += 1
            continue
        if garbage_size:
            print "{0} bytes of garbage".format(hex(garbage_size))
            garbage_size = 0
        print immed
    if garbage_size:
        print "{0} bytes of garbage".format(hex(garbage_size))

def dump_stack(ropinstr, assembler):
    """Dump last assembled gadget into a file"""
    format_struct = { 4:  "I",
                      2 :" H",
                      1 : "B"}
    if not ropinstr:
        print "no stack to dump"
        return
    res = ""
    filename = raw_input("> File to dump stack into ?: ")
    for immed in ropinstr.stack.values:
        #Handling RopStack.next_addr in stack (if end by retn)
        if immed.size == 0:
            immed = assembler.archi.Immediat(0x42dead42)
        print "{0} => {1}".format(immed.size, hex(immed.value))
        res += struct.pack(format_struct[immed.size], immed.value)

    f = open(filename, "wb")
    f.write(res)
    f.close()

def print_help(ropinstr, ass):
    """Give some Help"""
    print ("This shell allow you to assemble ROP Stack for the file" +
            "'{0}' of archi : <{1}>".format(ass.mapfile.f.name, ass.archi.name))
    print "To assemble an instruction use the following syntax :"
    print '"mnemo arg0,arg1,..!noreg1,noreg2,.."'
    print ("To list the instructions for archi <{0}> of command 'instrset'"
            .format(ass.archi.name))

def print_instrset(ropinstr, ass):
    """Give information about the available instruction set"""
    for mnemo, instr in ass.instr_set.items():
        print ""
        print '{0} :\n {1}'.format(mnemo, instr.__doc__)
        for atype, adoc in zip(instr.args, instr.args_doc):
            print "   '{1}' => {0}".format(normalize_atype(atype).__name__, adoc)

def normalize_atype(atype):
    """Return the original type of a RopInstr argument"""
    normalized = [Register, Immediat]
    for norm in normalized:
        if issubclass(atype, norm):
            return norm
    return atype


def launch_pdb(ropinstr, ass):
    """Debug : Launch python debugger"""
    import pdb
    pdb.set_trace()

Options = {
        'print' : print_stack,
        'dump' : dump_stack,
        'pdb' : launch_pdb,
        'help' : print_help,
        'instrset' : print_instrset,
        }
