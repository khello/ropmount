#!/usr/bin/python2

archi_name = "INTEL32"

class FakeMapFile(object):
    """ A Map file based on simple string with X86 opcode for debug"""

    def __init__(self, file=None):
        self.f = file
        self.__mapping = []
        self.archi_name = archi_name
        self.EIP = 0

    def define_mapping(self, mapping):
        """ define the mapping of FakeMapFile
        mapping arg = [(vaddr, "section")]"""
        self.__mapping = mapping

    # No file => No real offset
    def offset_from_vaddr(self, vaddr):
        return vaddr

    def get_mapping(self):
        res = []
        for start_vaddr, code in self.__mapping:
            res.append((start_vaddr, start_vaddr + len(code)))
        return res

    def read_from_vaddr(self, vaddr, size):
        for start_vaddr, code in self.__mapping:
            if start_vaddr <= vaddr <= start_vaddr + len(code):
                 return code[vaddr - start_vaddr: vaddr - start_vaddr + size]

