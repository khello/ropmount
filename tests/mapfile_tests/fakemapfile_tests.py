#!/usr/bin/python2

from tests.mapfile_tests.fakemapfile import FakeMapFile
from nose.tools import *


def test_simple_mapping():
    """  Fakemap test simple mapping """
    FMF = FakeMapFile()
    FMF.define_mapping([(0x1000, "A" * 0x42 )])
    mapping = FMF.get_mapping()
    assert_equal(mapping[0][0], 0x1000)
    assert_equal(mapping[0][1], 0x1042)



def test_double_mapping():
    """  Fakemap test double mapping """
    FMF = FakeMapFile()
    FMF.define_mapping([(0x1000, "A" * 0x42 ),(0x0, "A" * 0x100 )])
    mapping = FMF.get_mapping()
    assert_equal(mapping[0][0], 0x1000)
    assert_equal(mapping[0][1], 0x1042)
    assert_equal(mapping[1][0], 0x0)
    assert_equal(mapping[1][1], 0x100)

def test_read_from_vaddr():
    """  Fakemap test read from vaddr """
    FMF = FakeMapFile()
    s0 = "A" * 0x42 + "BBA"
    FMF.define_mapping([(0x1000, "A" * 0x42 + "BBA" ),(0x0,  "ABC" + "D" * 0x100 )])
    s = FMF.read_from_vaddr(1, 3)
    assert_equal(s, "BCD")
    s2 = FMF.read_from_vaddr(0x1041, 10)
    assert_equal(s2, "ABBA")
    s3 = FMF.read_from_vaddr(0x1000, 0x45 )
    assert_equal(s0, s3)

