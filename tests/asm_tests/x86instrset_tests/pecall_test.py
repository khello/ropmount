#!/usr/bin/python2

from nose.tools import *
from tests.x86gadget_mapfile import gen_x86assembler

def pecall_test():
    """peTest call"""
    gadgets =  ["add %esp, 0x8; pop %ecx;ret"]
    #declaring a function
    gadgets +=  [".global func"]
    gadgets +=  ["func:"]
    gadgets +=  ["nop"]
    #deref register
    gadgets +=  ["mov %ecx, [%ecx]; ret"]
    #ret on ecx
    gadgets +=  ["push %ecx;ret"]
    ass = gen_x86assembler(gadgets)
    x = ass.assemble("pecall func,1,0x42,0x69")
    print x
    assert x
    x = ass.assemble("pecall func,1,0x42,0x69,0x32,0x77")
    print x
    assert x
    x = ass.assemble("pecall func,0,0x42,0x69,0x32,0x77")
    assert not x
