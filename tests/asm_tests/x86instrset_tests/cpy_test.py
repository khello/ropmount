#!/usr/bin/python2

from nose.tools import *
from tests.x86gadget_mapfile import gen_x86assembler


def simple_cpy_test():
    """Test simple cpy"""
    gadgets = ["mov %edx, %ecx;ret"]
    gadgets += ["mov %edi, %edx;pop %edx;ret"]
    gadgets += ["mov %esi, %edi;pop %edi;ret"]
    gadgets += ["mov %edi, %esi;ret"]

    ass = gen_x86assembler(gadgets)
    x = ass.assemble("cpy edx, ecx")
    assert x

def return_cpy_test():
    """Test cpy with mov in two sens"""
    gadgets = ["mov %edi, %edx;pop %edx;ret"]
    gadgets += ["mov %esi, %edi;pop %edi;ret"]
    gadgets += ["mov %edi, %esi;ret"]

    ass = gen_x86assembler(gadgets)
    x = ass.assemble("cpy edx, edi")
    assert not x
    x = ass.assemble("cpy edi, esi")
    assert x
