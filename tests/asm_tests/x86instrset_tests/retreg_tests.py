#!/usr/bin/python2

from nose.tools import *
from tests.x86gadget_mapfile import gen_x86assembler

def retreg_simple_test():
    """ Test simple retreg """
    gadgets =  ["push %ecx; ret"]
    gadgets +=  ["jmp %edx; jmp 0x00; ret"]
    ass = gen_x86assembler(gadgets)
    x = ass.assemble("retreg edx")
    assert x
    x = ass.assemble("retreg ecx")
    assert x
    x = ass.assemble("retreg edi")
    assert not x


def retreg_simple_garbage_test():
    """ Test simple retreg with retn and ROP garbage"""
    gadgets =  ["push %ecx; inc %eax ;ret 0x4"]
    gadgets +=  ["jmp %edx; jmp 0x00; ret 0x4"]
    ass = gen_x86assembler(gadgets)
    x = ass.assemble("retreg edx")
    assert x
#TODO
#    x = ass.assemble("retreg ecx")
#    assert x
    x = ass.assemble("retreg edi")
    assert not x

def retreg_complex_test():
    """ Test complex retreg """
    gadgets =  ["push %ecx; ret"]
    gadgets +=  ["jmp %edx; jmp 0x00; ret"]
    gadgets += ["mov %ecx, %edi;pop %esi;ret"]
    gadgets += ["mov %edx, %esi;pop %esi;ret"]
    ass = gen_x86assembler(gadgets)
    x = ass.assemble("retreg edi")
    assert x
    x = ass.assemble("retreg esi")
    assert x

#TODO
#def retreg_complex_garbage_test():
#    """ Test complex retreg with retn and ROP garbage"""
#    gadgets =  ["push %ecx; "]
#    gadgets +=  ["jmp %edx; jmp 0x00; ret 0x4"]
#    gadgets += ["mov %ecx, %edi;pop %esi;ret"]
#    gadgets += ["mov %edx, %esi;pop %esi;ret"]
#    ass = gen_x86assembler(gadgets)
#    x = ass.assemble("retreg edi")
#    assert x
#    x = ass.assemble("retreg esi")
#    assert x

def retreg_noreg_complex_test():
    """ Test no_regs on complex retreg"""
    gadgets =  ["push %ecx; ret"]
    gadgets +=  ["jmp %edx; jmp 0x00; ret"]
    gadgets += ["mov %ecx, %edi;pop %esi;ret"]
    gadgets += ["mov %edx, %esi;pop %esi;ret"]
    ass = gen_x86assembler(gadgets)
    x = ass.assemble("retreg edi!edi")
    assert x
    x = ass.assemble("retreg esi!esi")
    assert not x

