#!/usr/bin/python2

from nose.tools import *
from tests.x86gadget_mapfile import gen_x86assembler
from ropmount.ropasm.assembler import RopAssembler


def simple_mov_test():
    """Test of a simple mov with ROP instr"""
    gadgets = ["mov %edx, %ecx; inc %eax;ret"]
    ass = gen_x86assembler(gadgets)
    x = ass.assemble("mov edx, ecx")
    assert x
    assert_equal(len(x.stack.values), 1)

#ecx => esi in 3 step
def complex_mov_test():
    """Test complex mov"""
    gadgets = ["mov %edx, %ecx;ret"]
    gadgets += ["mov %edi, %edx;ret"]
    gadgets += ["mov %esi, %edi;ret"]
    ass = gen_x86assembler(gadgets)
    x = ass.assemble("mov esi, ecx")
    assert x
    assert_equal(len(x.stack.values), 3)

#ecx => esi in 3 step
#ecx => esi in 3 step without edi
def simple_mov_noregs_test():
    """Test noreg on a simple mov """
    gadgets = ["mov %edx, %ecx; inc %eax;ret"]
    ass = gen_x86assembler(gadgets)
    x = ass.assemble("mov edx, ecx!eax")
    assert not x

def complex_mov_no_regs_test():
    """Test noreg on a complex mov """
    gadgets = ["mov %edx, %ecx;ret"]
    gadgets += ["mov %edi, %edx;ret"]
    gadgets += ["mov %esi, %edi;ret"]

    gadgets += ["mov %eax, %edx;ret"]
    gadgets += ["mov %ebx, %eax;ret"]
    gadgets += ["mov %esi, %ebx;ret"]
    ass = gen_x86assembler(gadgets)
    x = ass.assemble("mov esi, ecx!edi")
    assert x
    assert_equal(len(x.stack.values), 4)
    x = ass.assemble("mov esi, ecx!edi,ebx")
    assert not x

def use_armov_mov_test():
    """Test that a mov use armov RopInstr"""
    gadgets = ["pop %edx; ret"]
    gadgets += ["xor %edx, %ecx;ret"]
    ass = gen_x86assembler(gadgets)
    x = ass.assemble("mov edx, ecx")
    assert x

def use_ppmov_mov_test():
    """Test that a mov use ppmov RopInstr"""
    gadgets = ["push %ecx ; pop %edx ;ret"]
    ass = gen_x86assembler(gadgets)
    x = ass.assemble("mov edx, ecx")
    assert x

def use_armov_mov_test():
    """Test that a mov use pmov RopInstr"""
    gadgets = ["pushad ;ret"]
    gadgets += ["add %esp, 4 ;ret"]
    gadgets += ["pop %ebp ;ret"]
    gadgets += ["pop %edi ;ret"]
    gadgets += ["pop %ecx ; add %esp, 0x20 ;ret"]
    ass = gen_x86assembler(gadgets)
    x = ass.assemble("mov ecx, esp")
    assert x
