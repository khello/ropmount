#!/usr/bin/python2

from nose.tools import *
from tests.x86gadget_mapfile import gen_x86assembler
from ropmount.ropasm.assembler import RopAssembler


def simple_set_test():
    """ Test setting using a register using 'pop REG; ret'"""
    ass = gen_x86assembler(["pop %edx; ret"])
    x = ass.assemble("set edx,0x4242")
    assert x
    assert_equal(len(x.stack.values), 2)

def complex_set_test():
    """ Test setting using a register using 'pop REG; {,}ROP; ret'"""
    ass = gen_x86assembler(["pop %edx;pop %ecx; inc %eax; ret"])
    x = ass.assemble("set edx,0x4242")
    assert x
    # 1 addr + 1 value + 4 byte of garbage
    assert_equal(len(x.stack.values), 2 + 4)

def noreg_set_test():
    ass = gen_x86assembler(["pop %edx;pop %ecx; inc %eax; ret"])
    x = ass.assemble("set edx,0x4242!eax")
    assert not x
    x = ass.assemble("set edx,0x4242!ecx")
    assert not x

def simple_noreg_test():
    ass = gen_x86assembler(["pop %edx;pop %ecx; inc %edx; ret"])
    x = ass.assemble('set edx, 0x42')
    assert not x
