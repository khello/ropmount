#!/usr/bin/python2

from nose.tools import *
from tests.x86gadget_mapfile import gen_x86assembler


def simple_ppmov_test():
    """A simple pushpopmov test"""
    gadgets = ["push %edx; pop %ecx; inc %eax;ret 0x4"]
    ass = gen_x86assembler(gadgets)
    x = ass.assemble("ppmov ecx, edx")
    assert x
    print x.stack.values
    #addr + 0x4 for ret
    assert_equal(len(x.stack), 8)


def simple_noreg_test():
    """Test simple noregs on ppmov"""
    gadgets = ["push %edx; pop %ecx; inc %eax;ret 0x4"]
    ass = gen_x86assembler(gadgets)
    x = ass.assemble("ppmov ecx, edx!eax")
    assert not x

#Bug encoutered during dev
def complex_armov_test():
    """Test 'tricky' fail ppmov"""
    gadgets = ["push %ecx ; pop %eax; pop %edx ;ret 0x4"]
    ass = gen_x86assembler(gadgets)
    x = ass.assemble("ppmov eax, ecx")
    assert x
    assert_equal(len(x.stack), 12)
    x = ass.assemble("ppmov edx, ecx")
    assert not x

def second_simple_noreg_test():
    """Test second simple noregs on ppmov"""
    gadgets = ["push %edx; or %eax, 0xff; pop %ecx;ret 0x4"]
    ass = gen_x86assembler(gadgets)
    x = ass.assemble("ppmov edx, ecx!eax")
    assert not x

#test awas generating infinit loop during dev
def other_simple_test():
    """Test other complex push pop mov"""
    gadgets = ["push %edx;shr %ebx, 03;sub %eax, %ecx;shr %ecx, 02;add %eax, %ebx;shr %ebx, 05;sub %eax, %ecx;add %eax, %ebx;pop %ebx;ret 8"]
    ass = gen_x86assembler(gadgets)
    x = ass.assemble("ppmov ebx, edx")
    assert x
