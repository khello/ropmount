#!/usr/bin/python2

from nose.tools import *
from tests.x86gadget_mapfile import gen_x86assembler


def clean_test():
    """Test """
    gadgets =  ["add %esp, 80; ret"]
    gadgets +=  ["pop %eax; pop %ebx; ret"]
    ass = gen_x86assembler(gadgets)
    x = ass.assemble("clean 20")
    assert x
    x = ass.assemble("clean 2")
    assert x
    x = ass.assemble("clean 2!ebx")
    assert x
    #big stack cause it will use the add esp and put garbage
    assert len(x.stack) > 10
