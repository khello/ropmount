#!/usr/bin/python2

from nose.tools import *
from tests.x86gadget_mapfile import gen_x86assembler
from ropmount.ropasm.instrsets.ropinstr import RopStack


def stack_merge_test():
    gadgets  = ["mov %ecx, %edx;ret 0x1"]
    gadgets += ["pop %edx;ret"]
    ass = gen_x86assembler(gadgets)
    x1 = ass.assemble("mov ecx, edx")
    assert x1
    #Stack : ["mov addr", "next_addr(-1)", "garbage"]
    assert_equal(x1.stack.values[-2].value, -1)
    assert_equal(len(x1.stack.values), 3)

    x2 = ass.assemble("set edx,0xffffffff")
    assert x2
    #Stack : ["pop addr", "0xffffffff"]
    assert_equal(len(x2.stack.values), 2)

    x1.merge(x2)
    #Stack : ["mov addr", "pop addr", "garbage", "0xffffffff"]
    print x1.stack.values
    assert_equal(len(x1.stack.values), 4)
    assert_equal(x1.stack.values[1], x2.stack.values[0])
    assert not any([immed.value == -1 for immed in x1.stack.values])
