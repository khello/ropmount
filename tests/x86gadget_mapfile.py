#!/usr/bin/python2

import tempfile
import subprocess
import ropmount.mapfile as MapFile
from ropmount.ropasm.assembler import RopAssembler


header = [".intel_syntax\n",
          ".global _start\n",
          "_start :\n"]

file_name = "/tmp/gad.S"
output_file = "/tmp/out"

class X86GadgetMapFileFactory(object):
    def __init__(self):
        self.gadgets = []

    def add_gadget(self, gadget):
        self.gadgets.append(gadget)

    def generate_mapfile(self):
        #f = open(file_name, 'w+')
        #f_asm = tempfile.NamedTemporaryFile(suffix='.S')
        f_asm = open(file_name, 'w+')
        f_asm.writelines(header)
        for gad in self.gadgets:
            f_asm.write(gad + '\n')
        f_asm.flush()
        #f_bin = tempfile.NamedTemporaryFile()
        f_bin = open(output_file, 'w')
        args = ["gcc","-m32","-nostdlib" , "-o", f_bin.name, f_asm.name]
        print "Call : <{0}>".format(" ".join(args))
        subprocess.check_call(args)
        m = MapFile.create_mapfile(f_bin.name)
        f_asm.close()
        f_bin.close()
        return m


def gen_x86assembler(gadgets):
    x = X86GadgetMapFileFactory()
    for gad in gadgets:
        x.add_gadget(gad)
    mapf = x.generate_mapfile()
    return RopAssembler(mapf)
