#! /usr/bin/python2

import BeaEnginePython as Bea

instr = Bea.DISASM()

str_code = "\x89\x03\x81\xc4\x04\x01\x00\x00\x58\x5b\xc3"
code = Bea.create_string_buffer(str_code, len(str_code))


instr.EIP = Bea.addressof(code)

def Next(instr):
    return Bea.Disasm(Bea.addressof(instr))


