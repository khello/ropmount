from ropmount.disas.instr import InstrDisas

class Dummy(object):
    pass

class FakeInstrDisas(object):
    """ Fake InstrDisas with easy init for test"""
    def __init__(self, str_instr):
        self.Instruction = Dummy
        self.Instruction.Mnemonic = str_instr.split(' ')[0] + ' '
        self.CompleteInstr = str_instr
