#!/usr/bin/python2

from nose.tools import *
from ropmount.disas.gadget import GadgetDisas
from tests.disas_tests.fakeinstrdisas import FakeInstrDisas

instrs = ['int 0x80', 'ret']
l = map(FakeInstrDisas, instrs)
l[0].vaddr = 0

def simple_disas_Test():
    """ Test simple iter gadgetdisas"""
    # int 0x80; ret
    gdisas = GadgetDisas(l)
    gen = iter(gdisas)
    assert_equal(gen.next().CompleteInstr, instrs[0])
    assert_equal(gen.next().CompleteInstr, instrs[1])



def simple_reset_Test():
    """ Test gadgetdisas multiple iterator """
    # int 0x80; ret
    gdisas = GadgetDisas(l)
    gen = iter(gdisas)
    gen2 = iter(gdisas)
    assert_equal(gen.next().CompleteInstr, instrs[0])
    assert_equal(gen2.next().CompleteInstr, instrs[0])
    assert_equal(gen2.next().CompleteInstr, instrs[1])
    assert_equal(gen.next().CompleteInstr, instrs[1])
