#!/usr/bin/python2

from ropmount.ropfinding.filters.intfilter.asmparser import (ConstMatcher, RegMatcher,
                                                            MemAccessMatcher, InstrMatcher)
import ropmount.ropfinding.filters.intfilter.asmparser as AsmParser
import ropmount.disas as Disas
from nose.tools import *

AsmParser.Archi = Disas.archis["INTEL32"]
Archi = AsmParser.Archi


class TestConstMatcher(object):
    """ AsmParser Immed Test """
    def test_parse_immed(self):
        """ Test Immed parsing """
        i = ConstMatcher.parse('CONST')
        assert i
        i = ConstMatcher.parse('10')
        assert i
        j = ConstMatcher.parse('0xa')
        assert_equal(i.value,j.value)
        i = ConstMatcher.parse('test')
        assert not i
        i = ConstMatcher.parse('ANY')
        assert not i

    def test_match_immed(self):
        """ Test Immed matching """
        i = ConstMatcher.parse('CONST')
        for v in range(3):
            assert i.match(Archi.Immediat(v))
        i = ConstMatcher.parse('0xa')
        assert i.match(Archi.Immediat(10))
        assert not i.match(Archi.Immediat(9))

class TestRegMatcher(object):
    def test_RegX_parsing(self):
        """ Test RegX parsing """
        assert RegMatcher.parse('REG32')
        assert RegMatcher.parse('ESP')
        assert RegMatcher.parse('EAX')
        assert RegMatcher.parse('AX')

    def test_RegX_matching(self):
        """ Test RegX matching """
        r32 =  RegMatcher.parse('REG32')
        for r in map(Archi.Register, Archi.Register.valid32):
            assert r32.match(r)

        r32 =  RegMatcher.parse('EAX')
        assert r32.match(Archi.Register('EAX'))
        assert not r32.match(Archi.Register('AX'))
        assert not r32.match(Archi.Register('EDX'))


class TestMemAccess(object):
    def test_memaccess_parsing(self):
        """ Test MemAccess parsing """
        assert not MemAccessMatcher.parse('[]')
        assert not MemAccessMatcher.parse('ANY')
        assert MemAccessMatcher.parse('[ANY]')
        assert MemAccessMatcher.parse('[EAX]')
        assert not MemAccessMatcher.parse('[EAX + EDX]')
        assert not MemAccessMatcher.parse('[EAX + EDX + ECX]')
        assert not MemAccessMatcher.parse('[EAX + EDX + ECX + CONST]')
        assert not MemAccessMatcher.parse('[EDX * 1]')
        assert MemAccessMatcher.parse('[EAX + EDX * 1]')
        assert MemAccessMatcher.parse('[EAX + EDX * 2]')
        assert MemAccessMatcher.parse('[EAX + EDX * CONST]')
        assert not MemAccessMatcher.parse('[EAX + EDX * FAKE]')
        assert not MemAccessMatcher.parse('[EAX + BAD_REGISTER * 2]')
        assert not MemAccessMatcher.parse('[EAX + EDX * CONST + LOL]')
        assert not MemAccessMatcher.parse('[EAX + EDX * 3]')
        assert MemAccessMatcher.parse('[CONST]')
        assert MemAccessMatcher.parse('[EAX + CONST]')
        assert MemAccessMatcher.parse('[REG32 + REG32 * CONST + CONST]')
        assert not MemAccessMatcher.parse('[REG32 + REG32 + CONST]')
        assert MemAccessMatcher.parse('[REG32]')
        assert not MemAccessMatcher.parse('[REG]')


class TestInstr(object):
    def test_instr_parsing(self):
        """ Test Instr parsing """
        assert InstrMatcher.parse('ret')
        assert_raises(ValueError, InstrMatcher.parse, 'retn')
        assert_raises(ValueError, InstrMatcher.parse, 'mov CONST,CONST')
        assert InstrMatcher.parse('retn CONST')
        assert InstrMatcher.parse('mov REG32, REG32')
        assert InstrMatcher.parse('mov REG32, [CONST]')
        assert InstrMatcher.parse('ANY')

