#!/usr/bin/python2

from nose.tools import *
from ropmount.ropfinding.opcodefinder import GadgetValidator
from ropmount.disas.x86 import X86DisasDB



def make_DB(block):
    return X86DisasDB(block, 0)


#For this Test all backward gadget are valid
def Test_simple_next():
    """ Test simple validator next"""
    #int3; ret; xor edx,edx
    GV = iter(GadgetValidator(make_DB("\xcc\xc3\x31\xd2"), 2))
    GD = iter(GV.next())
    instr = GD.next()
    assert_equal(instr.mnemo, 'xor')
    GD = iter(GV.next())
    instr = GD.next()
    assert_equal(instr.mnemo, 'ret')
    GD = iter(GV.next())
    instr = GD.next()
    assert_equal(instr.mnemo, 'int3')
    instr = GD.next()
    assert_equal(instr.mnemo, 'ret')

def Test_double_next():
    """ Test next with one invalid disas """
    # xor; xor ; ret
    # gadget that begin with \xd2 consume ret => notvalid
    GV = iter(GadgetValidator(make_DB("\x31\xd2\x31\xd2\xc3"), 4))
    GD = iter(GV.next())
    instr = GD.next()
    assert_equal(instr.mnemo, 'ret')
    GD = iter(GV.next())
    instr = GD.next()
    assert_equal(instr.mnemo, 'xor')
    instr = GD.next()
    assert_equal(instr.mnemo, 'ret')
    GD = iter(GV.next())
    instr = GD.next()
    assert_equal(instr.mnemo, 'xor')
    instr = GD.next()
    assert_equal(instr.mnemo, 'xor')
    instr = GD.next()
    assert_equal(instr.mnemo, 'ret')


def Test_invalid():
    """Test an invalid instr disas"""
    GV = iter(GadgetValidator(make_DB("\x81\x81\xc3"), 2))
    GD = GV.next()
    try:
        GD = GV.next()
        assert False
    except StopIteration:
        return

def Test_real_gadget_finding():
    """ Test instr finding between legit instr"""
    #Test from Starcraft and hidden push esp; ret
    # add esp, 0x54 ; ret
    # push esp; ret hidden (\x54\xc3)
    GV = iter(GadgetValidator(make_DB("\x83\xc4\x54\xc3"), 3))
    GD = iter(GV.next())
    instr = GD.next()
    assert_equal(instr.mnemo, 'ret')
    GD = iter(GV.next())
    instr = GD.next()
    assert_equal(instr.mnemo, 'push')
    instr = GD.next()
    assert_equal(instr.mnemo, 'ret')
    GD = iter(GV.next())
    instr = GD.next()
    assert_equal(instr.mnemo, 'add')
    instr = GD.next()
    assert_equal(instr.mnemo, 'ret')
