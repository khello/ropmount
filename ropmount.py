#!/usr/bin/python2
""" The entry point of ropmount
    Command line parser that call the good front-end
"""

import argparse
from ropmount.ropdump import  Entry as EntryDump
from ropmount.asm import  Entry as EntryAss

parser = argparse.ArgumentParser(description='ropmount launch options')
parser.add_argument('filename', action='store', default='', help='Executable file to analyse')

parser.add_argument('--dump', nargs='+', metavar=['template'], help='Default option : dump all gadgets matching templates')
parser.add_argument('--ass',  action='store_const', dest='Entry', const=EntryAss,   help='Launch RopAsm assembler shell')
#Debug / Test option
parser.add_argument('--prof', action='store_true', dest='profiling', help='[DEBUG] launch with profiler')

#Need to find a cleaner way to check args
args = parser.parse_args()
if args.dump:
    if args.Entry != None:
        print "Can't specify --ass and --dump together"
        exit(1)
    args.Entry = EntryDump

entries_args = {
        EntryAss : lambda args : (args.filename,),
        EntryDump : lambda args : (args.filename, args.dump),
        }

if args.profiling:
    import cProfile
    cProfile.run('args.Entry(*entries_args[args.Entry](args))')
else:
    args.Entry(*entries_args[args.Entry](args))
